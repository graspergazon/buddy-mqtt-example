function myFunction() {
    document.getElementById("status").innerHTML = "";
  }


  var wsUri = "ws://localhost:8080";
//   var wsUri = "ws://localhost:51717";
  var output;
  var websocket;
  var rangePicker1;

  function init()
  {
      //image_output
      output = document.getElementById("output");
      configWebSocket();      
  }
  


function RequestRecording(recordingType, command)
{
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "Record", Message: command};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}












  function sendHelp() {
    sendTextFrame("help");
  }

  function configWebSocket()
  {
      websocket = new WebSocket(wsUri);
      websocket.binaryType = 'arraybuffer';
      websocket.onopen = function(evt) { onOpen(evt) };
      websocket.onclose = function(evt) { onClose(evt) };
      websocket.onmessage = function(evt) { onMessage(evt) };
      websocket.onerror = function(evt) { onError(evt) };
  }

  function sendTextFrame(message)
{
    if (websocket.readyState == WebSocket.OPEN)
    {
        emit("SENT: " + message);
        websocket.send(message);
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}
  
function requestSpeak()
{
    let txtElement = document.getElementById("txtToSpeak");
    
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "Talk", Message: txtElement.value};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}

function requestSpeakWithArg(text)
{
    // let txtElement = document.getElementById("txtToSpeak");
    
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "Talk", Message: text};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}


function requestSnapShot()
{
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "Snapshot"};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}



function showImage(message)
{
    // let message = JSON.parse(evt.data); 

    let image = message.ImageData;

    let imageStr = "<img src='" + image + "'>";

    // let imageStr = "";
    // for (let i = 0; i < image.length; i++) {
    //     imageStr += "<img src='" + image[i] + "'>";
    // } 

    let pre = document.createElement("p");   
    // pre.innerHTML = <br><h4>Matching Score: " + message.Score+ "</h4>";   
    let txt = "<div class='card' style='width: 700px;'><div class='card-body'><p class='card-text'>" + message.Info + "</p>"+ imageStr +"</div></div>";
    pre.innerHTML = txt;
    //output.appendChild(pre);
     //image_output
     imageOut = document.getElementById("image_output");
     imageOut.prepend(pre);
}




  function onOpen(evt)
  {


      var txt = "<div class='alert alert-success' role='alert'>Connected to Backend</div>"
        document.getElementById("status").innerHTML = txt;

    
    //   emit(txt);
      
  }
  
  function onClose(evt)
  {


      var txt = "<div class='alert alert-danger' role='alert'>Disconnected from backend!</div>"
      document.getElementById("status").innerHTML = txt;
    //   emit(txt);
  }
  
  
  
  function SetExpression()
  {
    let expression =  document.getElementById("expressionSelect").value;
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "FaceExpression", Index: parseInt(expression)};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }   
  }

  function SetFaceEvent()
  {
    let selectedEvent =  document.getElementById("eventSelect").value;
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "FaceEvent", Index: parseInt(selectedEvent)};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }   
  }

  function onMessage(evt)
  {
    let message = JSON.parse(evt.data); 

    if(message.MessageType == 'Image')
    {
        showImage(message);
    }
    else if(message.MessageType == 'Status')
    {
        ShowStatus(message.Status);
        AppendStatus(message.Status);
    }
    else if(message.MessageType == 'Face_response')
    {
        let subMessage = JSON.parse(message.Info); 
        ShowResponse(message.Info);
        // AppendStatus(message.Status);
    }

      //emit(message.data)
//        var c = document.getElementById('mycanvas');
//   var ctx = c.getContext('2d');
//   var imageData = ctx.createImageData(320, 240);
//   var pixels = imageData.data;
  
//   var buffer = new Uint8Array(message.data);
//   for (var i=0; i < pixels.length; i++) {
//   pixels[i] = buffer[i];
//   }
//   ctx.putImageData(imageData, 0, 0);
  
      
//   var p = document.getElementById('imagaDataLen');
//   p.textContent = pixels.byteLength + ' bytes';
  }
  
  function onError(evt)
  {
      var txt = "<div class='alert alert-warning' role='alert'>" + evt.data + "</div>"
      emit(txt);
  }

  function ShowStatus(statusTxt)
  {
      var txt = "<div class='alert alert-dark' role='alert'>" + statusTxt + "</div>"
      document.getElementById("statusAlt").innerHTML = txt;
     
  }

  function ShowResponse(txt)
  {
      var txt = "<div class='alert alert-dark' role='alert'>" + txt + "</div>"
      document.getElementById("faceResponse").innerHTML = txt;
     
  }

  function AppendStatus(statusTxt)
  {

    var pre = document.createElement("div","class='alert alert-dark' role='alert'");
    pre.innerHTML = statusTxt;
      var statusLog = document.getElementById("statusLog");
      statusLog.appendChild(pre);
     
  }

  function emit(message)
  {
      var pre = document.createElement("p");
      // pre.style.wordWrap = "break-word";
      pre.innerHTML = message;
      output.appendChild(pre);
  }

  window.addEventListener("load", init, false);
