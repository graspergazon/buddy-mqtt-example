﻿Simple Web-based Interface Example


Howto build:

0.  Install the .Net 8.0 SDK:
	https://learn.microsoft.com/en-us/dotnet/core/install/

1.  Build the example: 'dotnet build'
	This will build the application in './bin/Debug/net8.0/'

2.  Run the app: 'dotnet run ./bin/Debug/net8.0/WebBackend'

3.	Open a browser and go to http://localhost:8080
	or Open the /Dashboard/Dashboard.html file with a browser