﻿using libBuddyControl;
using Serilog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.WebSockets;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using static System.Net.Mime.MediaTypeNames;

namespace WebBackend
{

    public static class WebSocketServer
    {
        // note that Microsoft plans to deprecate HttpListener,
        // and for .NET Core they don't even support SSL/TLS
        // https://github.com/dotnet/platform-compat/issues/88


        private const string TOPIC_BUDDY_REQUEST = "buddy/request";
        private const string TOPIC_BUDDY_HEAD = "buddy/head";
        private const string TOPIC_BUDDY_BODY = "buddy/body";
        private const string TOPIC_BUDDY_VOICE = "buddy/voice";
        private const string TOPIC_BUDDY_RECORD = "buddy/record";


        private const string TOPIC_BUDDY_RESPONSE = "buddy/response";
        private const string TOPIC_BUDDY_DEBUG = "buddy/debug";
        private const string TOPIC_BUDDY_SNAPSHOT = "buddy/snapshot";


        private static HttpListener _listener;

        private static CancellationTokenSource SocketLoopTokenSource;
        private static CancellationTokenSource ListenerLoopTokenSource;

        private static int SocketCounter = 0;

        private static bool ServerIsRunning = true;

        private static MqttClient _mqttClient;
        private static string _mqttCliendId;

        // The key is a socket id
        private static ConcurrentDictionary<int, ConnectedClient> Clients = new ConcurrentDictionary<int, ConnectedClient>();
        private static string _voice = "nl-NL_EmmaVoice";

        public static async void MqttConnect(string server, int port)
        {
            _mqttClient = new MqttClient(server, port, false, null, null, MqttSslProtocols.None);
            _mqttClient.MqttMsgPublishReceived += _mqttClient_MqttMsgPublishReceived;
            _mqttClient.ConnectionClosed += _mqttClient_ConnectionClosed;

            _mqttCliendId = Guid.NewGuid().ToString();

            MqttConnectAndSubscribe();

            Console.WriteLine($"Connected to mqtt broker at {server} on port {port}");
            Log.Information($"Connected to mqtt broker at {server} on port {port}");
            await RunBashScript("/home/pi/vv/scripts/startBrowser.sh /home/pi/vv/firstpilot/Web/FirstPilot.html");

        }

        private static void _mqttClient_ConnectionClosed(object sender, EventArgs e)
        {
            Log.Information("Mqtt connection closed");

            if (_mqttClient == null)
                return;

            if (!_mqttClient.IsConnected) 
            {
                Reconnect();
            }
        }

        private static void MqttConnectAndSubscribe() 
        {

            _mqttClient.Connect(_mqttCliendId);

            string[] topics = new string[]
            {
                TOPIC_BUDDY_RESPONSE,
                TOPIC_BUDDY_DEBUG,
                TOPIC_BUDDY_SNAPSHOT

            };

            byte[] qosLevels = new byte[topics.Length];
            for (int i = 0; i < qosLevels.Length; i++)
            {
                qosLevels[i] = MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE;
            }


            _mqttClient.Subscribe(topics, qosLevels);

           
        }


        private static void Reconnect() 
        {
            if (_mqttClient.IsConnected)
                return;

            Log.Information($"Mqtt connection is down, trying to reconnect");

            MqttConnectAndSubscribe();

            if (_mqttClient.IsConnected)
            {
                Console.WriteLine($"Reconnected to mqtt broker");
                Log.Information($"Mqtt connection is down, trying to reconnect");
            }
        }

        private async static Task SendToAllClients(byte[] data) 
        {
            foreach (var item in Clients)
            {
                var client = item.Value;

                await client.Socket.SendAsync(new ArraySegment<byte>(data), WebSocketMessageType.Text,true, CancellationToken.None);
            }
        }

        private static async void _mqttClient_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            string topic = e.Topic;

            Log.Information($"Received data on topic {topic}");

            try
            {

                if (e.Topic.ToLower().Contains(TOPIC_BUDDY_SNAPSHOT))
                {
                    var imageData = e.Message;

                    string imageBase64 = string.Empty;
                    string img64 = Convert.ToBase64String(imageData);
                    imageBase64 = string.Format("data:image/jpg;base64,{0}", img64); //imagetype can be e.g. gif, jpeg, png etc.

                    DashBoardMessage message = new DashBoardMessage { ImageData = imageBase64, Info = "Test Image", MessageType = "Image" };

                    string json = JsonSerializer.Serialize<DashBoardMessage>(message);

                    await SendToAllClients(System.Text.Encoding.UTF8.GetBytes(json));
                }               
                else if (topic.ToLower().Contains(TOPIC_BUDDY_RESPONSE))
                {
                    string txt = System.Text.Encoding.UTF8.GetString(e.Message);

                    Log.Information($"received: {txt}");

                    DashBoardMessage message = new DashBoardMessage { MessageType = "Status", Status = txt };

                    string json = JsonSerializer.Serialize<DashBoardMessage>(message);

                    await SendToAllClients(System.Text.Encoding.UTF8.GetBytes(json));
                }
                
            }
            catch (Exception ex) 
            {
                Log.Error(ex, "Error processing mqtt data");
            }


        }

        private static async Task RunBashScript(string pathToScript) 
        {

            if (OperatingSystem.IsWindows())
                return;


            Log.Information($"Running script: {pathToScript}");


            await System.Threading.Tasks.Task.Run(new Action(() =>
            {

                try
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.UseShellExecute = false;
                    startInfo.FileName = "/bin/bash"; // Specify exe name.
                    startInfo.Arguments = pathToScript;

                    //SendStatus($"{DateTime.Now.ToString("HH:mm:ss yy-MM-dd")} Shutting down external Pi");


                    using (Process p = Process.Start(startInfo))
                    {
                        p.WaitForExit(5000);

                        if (p.HasExited)
                        {
                            //Log.Information($"Shutting down");

                        }
                    }
                }
                catch (Exception ex)
                {
                    //Log.Error(ex, "Error shutting down");


                }
            }));
        }

        public static void Start(string uriPrefix)
        {
            SocketLoopTokenSource = new CancellationTokenSource();
            ListenerLoopTokenSource = new CancellationTokenSource();
            _listener = new HttpListener();
            _listener.Prefixes.Add(uriPrefix);
            _listener.Start();
            if (_listener.IsListening)
            {
                Console.WriteLine("Connect browser for a basic echo-back web page.");
                Console.WriteLine($"Server listening: {uriPrefix}");
                // listen on a separate thread so that Listener.Stop can interrupt GetContextAsync
                Task.Run(() => ListenerProcessingLoopAsync().ConfigureAwait(false));
            }
            else
            {
                Console.WriteLine("Server failed to start.");
                Log.Error("Server failed to start.");
            }
        }

        public static async Task StopAsync()
        {
            if (_listener?.IsListening ?? false && ServerIsRunning)
            {
                Console.WriteLine("\nServer is stopping.");

                ServerIsRunning = false;            // prevent new connections during shutdown
                await CloseAllSocketsAsync();            // also cancels processing loop tokens (abort ReceiveAsync)
                ListenerLoopTokenSource.Cancel();   // safe to stop now that sockets are closed
                _listener.Stop();
                _listener.Close();
            }
        }

        private static async Task ListenerProcessingLoopAsync()
        {
            var cancellationToken = ListenerLoopTokenSource.Token;
            try
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    HttpListenerContext context = await _listener.GetContextAsync();
                    if (ServerIsRunning)
                    {
                        if (context.Request.IsWebSocketRequest)
                        {
                            // HTTP is only the initial connection; upgrade to a client-specific websocket
                            HttpListenerWebSocketContext wsContext = null;
                            try
                            {
                                wsContext = await context.AcceptWebSocketAsync(subProtocol: null);
                                int socketId = Interlocked.Increment(ref SocketCounter);
                                var client = new ConnectedClient(socketId, wsContext.WebSocket);
                                Clients.TryAdd(socketId, client);
                                Console.WriteLine($"Socket {socketId}: New connection.");
                                Log.Information($"Socket {client.SocketId}: New connection");
                                _ = Task.Run(() => SocketProcessingLoopAsync(client).ConfigureAwait(false));
                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex, "WebSocket upgrade failed");

                                // server error if upgrade from HTTP to WebSocket fails
                                context.Response.StatusCode = 500;
                                context.Response.StatusDescription = "WebSocket upgrade failed";
                                context.Response.Close();
                                return;
                            }
                        }
                        else
                        {
                            if (context.Request.AcceptTypes.Contains("text/html"))
                            {
                                Console.WriteLine("Sending HTML to client.");

                                ReadOnlyMemory<byte> HtmlPage = null;

                                string htmlFilePath = Path.Combine("Dashboard", "Dashboard.html");
                                string jsFilePath = Path.Combine("Dashboard", "Dashboard.js");

                                if (File.Exists(htmlFilePath))
                                {
                                    string htmlTxt = File.ReadAllText(htmlFilePath);

                                    string jsTxt = File.ReadAllText(jsFilePath);

                                    string fullTxt =  htmlTxt.Replace(@"src=""Dashboard.js"">", ">" + jsTxt);

                                    HtmlPage = new ReadOnlyMemory<byte>(Encoding.UTF8.GetBytes(fullTxt));
                                }
                                else 
                                {
                                    HtmlPage = new ReadOnlyMemory<byte>(Encoding.UTF8.GetBytes(SimpleHtmlClient.HTML));
                                }
                                
                                
                                context.Response.ContentType = "text/html; charset=utf-8";
                                context.Response.StatusCode = 200;
                                context.Response.StatusDescription = "OK";
                                context.Response.ContentLength64 = HtmlPage.Length;
                                await context.Response.OutputStream.WriteAsync(HtmlPage, CancellationToken.None);
                                await context.Response.OutputStream.FlushAsync(CancellationToken.None);
                            }
                            else
                            {
                                context.Response.StatusCode = 400;
                            }
                            context.Response.Close();
                        }
                    }
                    else
                    {
                        // HTTP 409 Conflict (with server's current state)
                        context.Response.StatusCode = 409;
                        context.Response.StatusDescription = "Server is shutting down";
                        context.Response.Close();
                        return;
                    }
                }
            }
            catch (HttpListenerException ex) when (ServerIsRunning)
            {
                Log.Error(ex, "Error while running server");
                Program.ReportException(ex);
            }
        }

        private static async Task SocketProcessingLoopAsync(ConnectedClient client)
        {
            var socket = client.Socket;
            var loopToken = SocketLoopTokenSource.Token;
            try
            {
                var buffer = WebSocket.CreateServerBuffer(4096);
                while (socket.State != WebSocketState.Closed && socket.State != WebSocketState.Aborted && !loopToken.IsCancellationRequested)
                {
                    var receiveResult = await client.Socket.ReceiveAsync(buffer, loopToken);
                    // if the token is cancelled while ReceiveAsync is blocking, the socket state changes to aborted and it can't be used
                    if (!loopToken.IsCancellationRequested)
                    {
                        // the client is notifying us that the connection will close; send acknowledgement
                        if (client.Socket.State == WebSocketState.CloseReceived && receiveResult.MessageType == WebSocketMessageType.Close)
                        {
                            Log.Information($"Socket {client.SocketId}: Acknowledging Close frame received from client");
                            Console.WriteLine($"Socket {client.SocketId}: Acknowledging Close frame received from client");
                            await socket.CloseOutputAsync(WebSocketCloseStatus.NormalClosure, "Acknowledge Close frame", CancellationToken.None);
                            // the socket state changes to closed at this point
                        }

                        // echo text or binary data to the broadcast queue
                        if (client.Socket.State == WebSocketState.Open)
                        {
                            if (receiveResult.MessageType == WebSocketMessageType.Text) 
                            {
                                try 
                                {

                                    byte[] data = new byte[receiveResult.Count];
                                    Array.Copy(buffer.Array,0, data, 0, data.Length);
                                    string jsonString = Encoding.Default.GetString(data);

                                    Console.WriteLine($"Received txt: {jsonString}");
                                    Log.Information($"Received txt from {client.SocketId} over websocket: {jsonString}");
                                    BuddyCommand? buddyCMD = JsonSerializer.Deserialize<BuddyCommand>(jsonString);

                                    if (!_mqttClient.IsConnected) 
                                    {
                                        
                                        Console.WriteLine($"Trying to reconnected to mqtt broker");
                                        Reconnect();
                                    }

                                    if (buddyCMD != null && buddyCMD.Command.Contains("Snapshot"))
                                    {
                                        if (_mqttClient != null && _mqttClient.IsConnected)
                                        {
                                            RecordingRequest request = new RecordingRequest(RecordingRequestType.SnapShot);

                                            SendMqttMessage(request.ToJsonString(), TOPIC_BUDDY_RECORD);
                                        }
                                    }
                                    else if (buddyCMD != null && buddyCMD.Command.Contains("Talk"))
                                    {
                                        if (_mqttClient != null && _mqttClient.IsConnected && !string.IsNullOrEmpty(buddyCMD.Message))
                                        {
                                            BuddyRequest request = new BuddyRequest(BuddyRequestType.Speech, buddyCMD.Message);

                                            SendMqttMessage(request.ToJsonString(), TOPIC_BUDDY_REQUEST);
                                        }
                                    }									
									else if (buddyCMD != null && buddyCMD.Command.Contains("FaceExpression"))
                                    {
                                        BuddyRequest request = new BuddyRequest
                                        {
                                            RequestType = (int)BuddyRequestType.FacialExpression,
                                            Payload = string.Empty,
                                            ExpressionType = buddyCMD.Index
                                        };

                                        SendMqttMessage(request.ToJsonString(), TOPIC_BUDDY_REQUEST);
                                    }
									else if (buddyCMD != null && buddyCMD.Command.Contains("FaceEvent"))
                                    {
                                        BuddyRequest request = new BuddyRequest
                                        {
                                            RequestType = (int)BuddyRequestType.FacialEvent,
                                            Payload = string.Empty,
                                            ExpressionType = buddyCMD.Index
                                        };

                                        SendMqttMessage(request.ToJsonString(), TOPIC_BUDDY_REQUEST);
                                    } 
                                    else if (buddyCMD != null && buddyCMD.Command.Contains("SetVoice"))
                                    {
                                        
                                    }
                                    else if (buddyCMD != null && buddyCMD.Command.Contains("Record"))
                                    {
                                        if(!string.IsNullOrEmpty(buddyCMD.Message)) 
                                        {
                                            RecordingRequest request = null;

                                            if (buddyCMD.Message.ToLower().Contains("stop"))
                                            {
                                                request = new RecordingRequest(RecordingRequestType.Stop, new Size(1280, 720), 30);                                               
                                            }
                                            else if (buddyCMD.Message.ToLower().Contains("start")) 
                                            {
                                                request = new RecordingRequest(RecordingRequestType.Start, new Size(1280, 720), 30);                                                
                                            }

                                            if(request != null) 
                                                SendMqttMessage(request.ToJsonString(), TOPIC_BUDDY_RECORD);
                                        }

                                    }                                    
                                    else
                                    {
                                        
                                    }
                                }
                                catch (Exception ex) 
                                {
                                    Console.WriteLine($"Received txt is not valid json");
                                }
                            }


                            //Console.WriteLine($"Socket {client.SocketId}: Received {receiveResult.MessageType} frame ({receiveResult.Count} bytes).");
                            //Console.WriteLine($"Socket {client.SocketId}: Echoing data to client.");
                            //await socket.SendAsync(new ArraySegment<byte>(buffer.Array, 0, receiveResult.Count), receiveResult.MessageType, receiveResult.EndOfMessage, CancellationToken.None);
                        }
                    }
                }
            }
            catch (OperationCanceledException)
            {
                // normal upon task/token cancellation, disregard
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Socket {client.SocketId}:");
                Program.ReportException(ex);
            }
            finally
            {
                Console.WriteLine($"Socket {client.SocketId}: Ended processing loop in state {socket.State}");

                // don't leave the socket in any potentially connected state
                if (client.Socket.State != WebSocketState.Closed)
                    client.Socket.Abort();

                // by this point the socket is closed or aborted, the ConnectedClient object is useless
                if (Clients.TryRemove(client.SocketId, out _))
                    socket.Dispose();
            }
        }

        private static async Task CloseAllSocketsAsync()
        {
            // We can't dispose the sockets until the processing loops are terminated,
            // but terminating the loops will abort the sockets, preventing graceful closing.
            var disposeQueue = new List<WebSocket>(Clients.Count);

            while (Clients.Count > 0)
            {
                var client = Clients.ElementAt(0).Value;
                Console.WriteLine($"Closing Socket {client.SocketId}");

                Console.WriteLine("... ending broadcast loop");

                if (client.Socket.State != WebSocketState.Open)
                {
                    Console.WriteLine($"... socket not open, state = {client.Socket.State}");
                }
                else
                {
                    var timeout = new CancellationTokenSource(Program.CLOSE_SOCKET_TIMEOUT_MS);
                    try
                    {
                        Console.WriteLine("... starting close handshake");
                        await client.Socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing", timeout.Token);
                    }
                    catch (OperationCanceledException ex)
                    {
                        Program.ReportException(ex);
                        // normal upon task/token cancellation, disregard
                    }
                }

                if (Clients.TryRemove(client.SocketId, out _))
                {
                    // only safe to Dispose once, so only add it if this loop can't process it again
                    disposeQueue.Add(client.Socket);
                }

                Console.WriteLine("... done");
            }

            // now that they're all closed, terminate the blocking ReceiveAsync calls in the SocketProcessingLoop threads
            SocketLoopTokenSource.Cancel();

            // dispose all resources
            foreach (var socket in disposeQueue)
                socket.Dispose();
        }

        private static void SendMqttMessage(string message, string topic)
        {

            if (_mqttClient == null)
            {
                return;
            }

            try
            {
                if (_mqttClient.IsConnected)
                {

                    _mqttClient.Publish(topic, System.Text.Encoding.UTF8.GetBytes(message));
                }
                else
                {
                    string clientId = Guid.NewGuid().ToString();
                    _mqttClient.Connect(clientId);
                    _mqttClient.Publish(topic, System.Text.Encoding.UTF8.GetBytes(message));
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
        }

    }
}
