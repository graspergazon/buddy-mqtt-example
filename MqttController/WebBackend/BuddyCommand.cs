﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebBackend
{
    public class BuddyCommand
    {
        public string Command { get; set; } //
        public string Message { get; set; }

        public int Index { get; set; }
    }

    public class DashBoardMessage
    {
        public string MessageType { get; set; }
        public string ImageData { get; set; }
        public string Info { get; set; }
        public string Status { get; set; }

    }
}
