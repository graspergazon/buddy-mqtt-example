﻿namespace ControllerApp
{
    partial class MainWindow
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            menuStrip1 = new MenuStrip();
            fileToolStripMenuItem = new ToolStripMenuItem();
            connectionToolStripMenuItem = new ToolStripMenuItem();
            connectToBrokerToolStripMenuItem = new ToolStripMenuItem();
            getStatusToolStripMenuItem = new ToolStripMenuItem();
            statusStrip1 = new StatusStrip();
            toolStripStatusLabel1 = new ToolStripStatusLabel();
            tableLayoutPanel1 = new TableLayoutPanel();
            tabControl1 = new TabControl();
            tabSpeech = new TabPage();
            tableLayoutPanel2 = new TableLayoutPanel();
            richTextBoxSpeech = new RichTextBox();
            panel1 = new Panel();
            lblVoice = new Label();
            buttonShutUp = new Button();
            trackVoiceSpeed = new TrackBar();
            trackVol = new TrackBar();
            trackVoicePitch = new TrackBar();
            bttnTalk = new Button();
            comboBoxVoices = new ComboBox();
            buttonVoice = new Button();
            groupBox1 = new GroupBox();
            radioHappy = new RadioButton();
            radioAngry = new RadioButton();
            radioNeutral = new RadioButton();
            radioNoExpression = new RadioButton();
            tabCamera = new TabPage();
            tableLayoutPanel3 = new TableLayoutPanel();
            panel3 = new Panel();
            bttnSnapshot = new Button();
            buttonStartRecording = new Button();
            buttonStopRecording = new Button();
            pictureBoxSnapShot = new PictureBox();
            tabFace = new TabPage();
            tableLayoutPanel4 = new TableLayoutPanel();
            listBoxExpression = new ListBox();
            listBoxFaceEvents = new ListBox();
            panel4 = new Panel();
            bttnChangeLED = new Button();
            textLEDClr = new TextBox();
            tableLayoutPanel5 = new TableLayoutPanel();
            richTextBoxLog = new RichTextBox();
            panel2 = new Panel();
            buttonClear = new Button();
            menuStrip1.SuspendLayout();
            statusStrip1.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            tabControl1.SuspendLayout();
            tabSpeech.SuspendLayout();
            tableLayoutPanel2.SuspendLayout();
            panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)trackVoiceSpeed).BeginInit();
            ((System.ComponentModel.ISupportInitialize)trackVol).BeginInit();
            ((System.ComponentModel.ISupportInitialize)trackVoicePitch).BeginInit();
            groupBox1.SuspendLayout();
            tabCamera.SuspendLayout();
            tableLayoutPanel3.SuspendLayout();
            panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBoxSnapShot).BeginInit();
            tabFace.SuspendLayout();
            tableLayoutPanel4.SuspendLayout();
            panel4.SuspendLayout();
            tableLayoutPanel5.SuspendLayout();
            panel2.SuspendLayout();
            SuspendLayout();
            // 
            // menuStrip1
            // 
            menuStrip1.Items.AddRange(new ToolStripItem[] { fileToolStripMenuItem, connectionToolStripMenuItem });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(1160, 24);
            menuStrip1.TabIndex = 0;
            menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            fileToolStripMenuItem.Size = new Size(37, 20);
            fileToolStripMenuItem.Text = "File";
            // 
            // connectionToolStripMenuItem
            // 
            connectionToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { connectToBrokerToolStripMenuItem, getStatusToolStripMenuItem });
            connectionToolStripMenuItem.Name = "connectionToolStripMenuItem";
            connectionToolStripMenuItem.Size = new Size(81, 20);
            connectionToolStripMenuItem.Text = "Connection";
            // 
            // connectToBrokerToolStripMenuItem
            // 
            connectToBrokerToolStripMenuItem.Name = "connectToBrokerToolStripMenuItem";
            connectToBrokerToolStripMenuItem.Size = new Size(170, 22);
            connectToBrokerToolStripMenuItem.Text = "Connect to Broker";
            connectToBrokerToolStripMenuItem.Click += connectToBrokerToolStripMenuItem_Click;
            // 
            // getStatusToolStripMenuItem
            // 
            getStatusToolStripMenuItem.Name = "getStatusToolStripMenuItem";
            getStatusToolStripMenuItem.Size = new Size(170, 22);
            getStatusToolStripMenuItem.Text = "Get Status";
            getStatusToolStripMenuItem.Click += getStatusToolStripMenuItem_Click;
            // 
            // statusStrip1
            // 
            statusStrip1.Items.AddRange(new ToolStripItem[] { toolStripStatusLabel1 });
            statusStrip1.Location = new Point(0, 545);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Size = new Size(1160, 22);
            statusStrip1.TabIndex = 1;
            statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            toolStripStatusLabel1.Size = new Size(126, 17);
            toolStripStatusLabel1.Text = "Status: Not Connected";
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 2;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 62.6724129F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 37.3275871F));
            tableLayoutPanel1.Controls.Add(tabControl1, 0, 0);
            tableLayoutPanel1.Controls.Add(tableLayoutPanel5, 1, 0);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(0, 24);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 1;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.Size = new Size(1160, 521);
            tableLayoutPanel1.TabIndex = 2;
            // 
            // tabControl1
            // 
            tabControl1.Controls.Add(tabSpeech);
            tabControl1.Controls.Add(tabCamera);
            tabControl1.Controls.Add(tabFace);
            tabControl1.Dock = DockStyle.Fill;
            tabControl1.Location = new Point(3, 3);
            tabControl1.Name = "tabControl1";
            tabControl1.SelectedIndex = 0;
            tabControl1.Size = new Size(721, 515);
            tabControl1.TabIndex = 3;
            // 
            // tabSpeech
            // 
            tabSpeech.Controls.Add(tableLayoutPanel2);
            tabSpeech.Location = new Point(4, 24);
            tabSpeech.Name = "tabSpeech";
            tabSpeech.Padding = new Padding(3);
            tabSpeech.Size = new Size(713, 487);
            tabSpeech.TabIndex = 0;
            tabSpeech.Text = "Speech";
            tabSpeech.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2.ColumnCount = 2;
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 81.61245F));
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 18.3875523F));
            tableLayoutPanel2.Controls.Add(richTextBoxSpeech, 0, 0);
            tableLayoutPanel2.Controls.Add(panel1, 0, 1);
            tableLayoutPanel2.Dock = DockStyle.Fill;
            tableLayoutPanel2.Location = new Point(3, 3);
            tableLayoutPanel2.Name = "tableLayoutPanel2";
            tableLayoutPanel2.RowCount = 2;
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel2.Size = new Size(707, 481);
            tableLayoutPanel2.TabIndex = 0;
            // 
            // richTextBoxSpeech
            // 
            richTextBoxSpeech.Dock = DockStyle.Fill;
            richTextBoxSpeech.Location = new Point(3, 3);
            richTextBoxSpeech.Name = "richTextBoxSpeech";
            richTextBoxSpeech.Size = new Size(571, 234);
            richTextBoxSpeech.TabIndex = 0;
            richTextBoxSpeech.Text = "";
            // 
            // panel1
            // 
            panel1.Controls.Add(lblVoice);
            panel1.Controls.Add(buttonShutUp);
            panel1.Controls.Add(trackVoiceSpeed);
            panel1.Controls.Add(trackVol);
            panel1.Controls.Add(trackVoicePitch);
            panel1.Controls.Add(bttnTalk);
            panel1.Controls.Add(comboBoxVoices);
            panel1.Controls.Add(buttonVoice);
            panel1.Controls.Add(groupBox1);
            panel1.Dock = DockStyle.Fill;
            panel1.Location = new Point(3, 243);
            panel1.Name = "panel1";
            panel1.Size = new Size(571, 235);
            panel1.TabIndex = 1;
            // 
            // lblVoice
            // 
            lblVoice.AutoSize = true;
            lblVoice.Location = new Point(154, 206);
            lblVoice.Name = "lblVoice";
            lblVoice.Size = new Size(13, 15);
            lblVoice.TabIndex = 16;
            lblVoice.Text = "..";
            // 
            // buttonShutUp
            // 
            buttonShutUp.Location = new Point(12, 42);
            buttonShutUp.Name = "buttonShutUp";
            buttonShutUp.Size = new Size(113, 23);
            buttonShutUp.TabIndex = 10;
            buttonShutUp.Text = "Stop Talking";
            buttonShutUp.UseVisualStyleBackColor = true;
            buttonShutUp.Click += buttonShutUp_Click;
            // 
            // trackVoiceSpeed
            // 
            trackVoiceSpeed.Location = new Point(154, 56);
            trackVoiceSpeed.Name = "trackVoiceSpeed";
            trackVoiceSpeed.Size = new Size(203, 45);
            trackVoiceSpeed.TabIndex = 13;
            // 
            // trackVol
            // 
            trackVol.Location = new Point(154, 107);
            trackVol.Name = "trackVol";
            trackVol.Size = new Size(203, 45);
            trackVol.TabIndex = 15;
            // 
            // trackVoicePitch
            // 
            trackVoicePitch.Location = new Point(152, 158);
            trackVoicePitch.Name = "trackVoicePitch";
            trackVoicePitch.Size = new Size(203, 45);
            trackVoicePitch.TabIndex = 14;
            // 
            // bttnTalk
            // 
            bttnTalk.Location = new Point(12, 15);
            bttnTalk.Name = "bttnTalk";
            bttnTalk.Size = new Size(75, 23);
            bttnTalk.TabIndex = 8;
            bttnTalk.Text = "Talk";
            bttnTalk.UseVisualStyleBackColor = true;
            bttnTalk.Click += bttnTalk_Click;
            // 
            // comboBoxVoices
            // 
            comboBoxVoices.FormattingEnabled = true;
            comboBoxVoices.Location = new Point(223, 15);
            comboBoxVoices.Name = "comboBoxVoices";
            comboBoxVoices.Size = new Size(121, 23);
            comboBoxVoices.TabIndex = 11;
            // 
            // buttonVoice
            // 
            buttonVoice.Location = new Point(361, 170);
            buttonVoice.Name = "buttonVoice";
            buttonVoice.Size = new Size(125, 23);
            buttonVoice.TabIndex = 12;
            buttonVoice.Text = "Update Voice";
            buttonVoice.UseVisualStyleBackColor = true;
            buttonVoice.Click += buttonVoice_Click;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(radioHappy);
            groupBox1.Controls.Add(radioAngry);
            groupBox1.Controls.Add(radioNeutral);
            groupBox1.Controls.Add(radioNoExpression);
            groupBox1.Location = new Point(363, 6);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(123, 127);
            groupBox1.TabIndex = 9;
            groupBox1.TabStop = false;
            groupBox1.Text = "Expression";
            // 
            // radioHappy
            // 
            radioHappy.AutoSize = true;
            radioHappy.Location = new Point(17, 91);
            radioHappy.Name = "radioHappy";
            radioHappy.Size = new Size(94, 19);
            radioHappy.TabIndex = 3;
            radioHappy.Text = "Speak Happy";
            radioHappy.UseVisualStyleBackColor = true;
            // 
            // radioAngry
            // 
            radioAngry.AutoSize = true;
            radioAngry.Location = new Point(17, 66);
            radioAngry.Name = "radioAngry";
            radioAngry.Size = new Size(91, 19);
            radioAngry.TabIndex = 2;
            radioAngry.Text = "Speak Angry";
            radioAngry.UseVisualStyleBackColor = true;
            // 
            // radioNeutral
            // 
            radioNeutral.AutoSize = true;
            radioNeutral.Location = new Point(17, 41);
            radioNeutral.Name = "radioNeutral";
            radioNeutral.Size = new Size(98, 19);
            radioNeutral.TabIndex = 1;
            radioNeutral.Text = "Speak Neutral";
            radioNeutral.UseVisualStyleBackColor = true;
            // 
            // radioNoExpression
            // 
            radioNoExpression.AutoSize = true;
            radioNoExpression.Checked = true;
            radioNoExpression.Location = new Point(17, 16);
            radioNoExpression.Name = "radioNoExpression";
            radioNoExpression.Size = new Size(100, 19);
            radioNoExpression.TabIndex = 0;
            radioNoExpression.TabStop = true;
            radioNoExpression.Text = "No Expression";
            radioNoExpression.UseVisualStyleBackColor = true;
            // 
            // tabCamera
            // 
            tabCamera.Controls.Add(tableLayoutPanel3);
            tabCamera.Location = new Point(4, 24);
            tabCamera.Name = "tabCamera";
            tabCamera.Padding = new Padding(3);
            tabCamera.Size = new Size(713, 487);
            tabCamera.TabIndex = 1;
            tabCamera.Text = "Camera";
            tabCamera.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            tableLayoutPanel3.ColumnCount = 2;
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 21.92362F));
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 78.07638F));
            tableLayoutPanel3.Controls.Add(panel3, 0, 0);
            tableLayoutPanel3.Controls.Add(pictureBoxSnapShot, 1, 0);
            tableLayoutPanel3.Dock = DockStyle.Fill;
            tableLayoutPanel3.Location = new Point(3, 3);
            tableLayoutPanel3.Name = "tableLayoutPanel3";
            tableLayoutPanel3.RowCount = 2;
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Percent, 79.83368F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Percent, 20.16632F));
            tableLayoutPanel3.Size = new Size(707, 481);
            tableLayoutPanel3.TabIndex = 0;
            // 
            // panel3
            // 
            panel3.Controls.Add(bttnSnapshot);
            panel3.Controls.Add(buttonStartRecording);
            panel3.Controls.Add(buttonStopRecording);
            panel3.Dock = DockStyle.Fill;
            panel3.Location = new Point(3, 3);
            panel3.Name = "panel3";
            panel3.Size = new Size(148, 378);
            panel3.TabIndex = 0;
            // 
            // bttnSnapshot
            // 
            bttnSnapshot.Location = new Point(15, 326);
            bttnSnapshot.Name = "bttnSnapshot";
            bttnSnapshot.Size = new Size(119, 23);
            bttnSnapshot.TabIndex = 2;
            bttnSnapshot.Text = "Request Snapshot";
            bttnSnapshot.UseVisualStyleBackColor = true;
            bttnSnapshot.Click += bttnSnapshot_Click;
            // 
            // buttonStartRecording
            // 
            buttonStartRecording.Location = new Point(15, 19);
            buttonStartRecording.Name = "buttonStartRecording";
            buttonStartRecording.Size = new Size(108, 23);
            buttonStartRecording.TabIndex = 1;
            buttonStartRecording.Text = "Start Recording";
            buttonStartRecording.UseVisualStyleBackColor = true;
            buttonStartRecording.Click += buttonStartRecording_Click;
            // 
            // buttonStopRecording
            // 
            buttonStopRecording.Location = new Point(15, 57);
            buttonStopRecording.Name = "buttonStopRecording";
            buttonStopRecording.Size = new Size(108, 23);
            buttonStopRecording.TabIndex = 0;
            buttonStopRecording.Text = "Stop Recording";
            buttonStopRecording.UseVisualStyleBackColor = true;
            buttonStopRecording.Click += buttonStopRecording_Click;
            // 
            // pictureBoxSnapShot
            // 
            pictureBoxSnapShot.Dock = DockStyle.Fill;
            pictureBoxSnapShot.Location = new Point(157, 3);
            pictureBoxSnapShot.Name = "pictureBoxSnapShot";
            pictureBoxSnapShot.Size = new Size(547, 378);
            pictureBoxSnapShot.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBoxSnapShot.TabIndex = 1;
            pictureBoxSnapShot.TabStop = false;
            // 
            // tabFace
            // 
            tabFace.Controls.Add(tableLayoutPanel4);
            tabFace.Location = new Point(4, 24);
            tabFace.Name = "tabFace";
            tabFace.Padding = new Padding(3);
            tabFace.Size = new Size(713, 487);
            tabFace.TabIndex = 2;
            tabFace.Text = "Face";
            tabFace.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            tableLayoutPanel4.ColumnCount = 2;
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel4.Controls.Add(listBoxExpression, 0, 0);
            tableLayoutPanel4.Controls.Add(listBoxFaceEvents, 1, 0);
            tableLayoutPanel4.Controls.Add(panel4, 0, 1);
            tableLayoutPanel4.Dock = DockStyle.Fill;
            tableLayoutPanel4.Location = new Point(3, 3);
            tableLayoutPanel4.Name = "tableLayoutPanel4";
            tableLayoutPanel4.RowCount = 2;
            tableLayoutPanel4.RowStyles.Add(new RowStyle(SizeType.Percent, 85.44698F));
            tableLayoutPanel4.RowStyles.Add(new RowStyle(SizeType.Percent, 14.5530148F));
            tableLayoutPanel4.Size = new Size(707, 481);
            tableLayoutPanel4.TabIndex = 0;
            // 
            // listBoxExpression
            // 
            listBoxExpression.Dock = DockStyle.Fill;
            listBoxExpression.FormattingEnabled = true;
            listBoxExpression.ItemHeight = 15;
            listBoxExpression.Location = new Point(3, 3);
            listBoxExpression.Name = "listBoxExpression";
            listBoxExpression.Size = new Size(347, 405);
            listBoxExpression.TabIndex = 1;
            listBoxExpression.SelectedIndexChanged += listBoxExpression_SelectedIndexChanged;
            // 
            // listBoxFaceEvents
            // 
            listBoxFaceEvents.Dock = DockStyle.Fill;
            listBoxFaceEvents.FormattingEnabled = true;
            listBoxFaceEvents.ItemHeight = 15;
            listBoxFaceEvents.Location = new Point(356, 3);
            listBoxFaceEvents.Name = "listBoxFaceEvents";
            listBoxFaceEvents.Size = new Size(348, 405);
            listBoxFaceEvents.TabIndex = 2;
            listBoxFaceEvents.SelectedIndexChanged += listBoxFaceEvents_SelectedIndexChanged;
            // 
            // panel4
            // 
            panel4.Controls.Add(bttnChangeLED);
            panel4.Controls.Add(textLEDClr);
            panel4.Dock = DockStyle.Fill;
            panel4.Location = new Point(3, 414);
            panel4.Name = "panel4";
            panel4.Size = new Size(347, 64);
            panel4.TabIndex = 3;
            // 
            // bttnChangeLED
            // 
            bttnChangeLED.Location = new Point(205, 20);
            bttnChangeLED.Name = "bttnChangeLED";
            bttnChangeLED.Size = new Size(129, 23);
            bttnChangeLED.TabIndex = 1;
            bttnChangeLED.Text = "Change Color";
            bttnChangeLED.UseVisualStyleBackColor = true;
            bttnChangeLED.Click += bttnChangeLED_Click;
            // 
            // textLEDClr
            // 
            textLEDClr.Location = new Point(22, 20);
            textLEDClr.Name = "textLEDClr";
            textLEDClr.Size = new Size(74, 23);
            textLEDClr.TabIndex = 0;
            textLEDClr.Text = "#FF00FF";
            // 
            // tableLayoutPanel5
            // 
            tableLayoutPanel5.ColumnCount = 1;
            tableLayoutPanel5.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel5.Controls.Add(richTextBoxLog, 0, 0);
            tableLayoutPanel5.Controls.Add(panel2, 0, 1);
            tableLayoutPanel5.Dock = DockStyle.Fill;
            tableLayoutPanel5.Location = new Point(730, 3);
            tableLayoutPanel5.Name = "tableLayoutPanel5";
            tableLayoutPanel5.RowCount = 2;
            tableLayoutPanel5.RowStyles.Add(new RowStyle(SizeType.Percent, 92.23301F));
            tableLayoutPanel5.RowStyles.Add(new RowStyle(SizeType.Percent, 7.76699F));
            tableLayoutPanel5.Size = new Size(427, 515);
            tableLayoutPanel5.TabIndex = 4;
            // 
            // richTextBoxLog
            // 
            richTextBoxLog.Dock = DockStyle.Fill;
            richTextBoxLog.Location = new Point(3, 3);
            richTextBoxLog.Name = "richTextBoxLog";
            richTextBoxLog.Size = new Size(421, 469);
            richTextBoxLog.TabIndex = 5;
            richTextBoxLog.Text = "";
            // 
            // panel2
            // 
            panel2.Controls.Add(buttonClear);
            panel2.Dock = DockStyle.Fill;
            panel2.Location = new Point(3, 478);
            panel2.Name = "panel2";
            panel2.Size = new Size(421, 34);
            panel2.TabIndex = 6;
            // 
            // buttonClear
            // 
            buttonClear.Location = new Point(31, 4);
            buttonClear.Name = "buttonClear";
            buttonClear.Size = new Size(116, 23);
            buttonClear.TabIndex = 0;
            buttonClear.Text = "Clear Log";
            buttonClear.UseVisualStyleBackColor = true;
            buttonClear.Click += buttonClear_Click;
            // 
            // MainWindow
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1160, 567);
            Controls.Add(tableLayoutPanel1);
            Controls.Add(statusStrip1);
            Controls.Add(menuStrip1);
            MainMenuStrip = menuStrip1;
            Name = "MainWindow";
            Text = "MQTT Controller";
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            statusStrip1.ResumeLayout(false);
            statusStrip1.PerformLayout();
            tableLayoutPanel1.ResumeLayout(false);
            tabControl1.ResumeLayout(false);
            tabSpeech.ResumeLayout(false);
            tableLayoutPanel2.ResumeLayout(false);
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)trackVoiceSpeed).EndInit();
            ((System.ComponentModel.ISupportInitialize)trackVol).EndInit();
            ((System.ComponentModel.ISupportInitialize)trackVoicePitch).EndInit();
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            tabCamera.ResumeLayout(false);
            tableLayoutPanel3.ResumeLayout(false);
            panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)pictureBoxSnapShot).EndInit();
            tabFace.ResumeLayout(false);
            tableLayoutPanel4.ResumeLayout(false);
            panel4.ResumeLayout(false);
            panel4.PerformLayout();
            tableLayoutPanel5.ResumeLayout(false);
            panel2.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private MenuStrip menuStrip1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel toolStripStatusLabel1;
        private ToolStripMenuItem connectionToolStripMenuItem;
        private ToolStripMenuItem connectToBrokerToolStripMenuItem;
        private ToolStripMenuItem getStatusToolStripMenuItem;
        private TableLayoutPanel tableLayoutPanel1;
        private TabControl tabControl1;
        private TabPage tabSpeech;
        private TableLayoutPanel tableLayoutPanel2;
        private RichTextBox richTextBoxSpeech;
        private TabPage tabCamera;
        private TableLayoutPanel tableLayoutPanel3;
        private TabPage tabFace;
        private TableLayoutPanel tableLayoutPanel4;
        private ListBox listBoxExpression;
        private ListBox listBoxFaceEvents;
        private Panel panel1;
        private Button buttonShutUp;
        private TrackBar trackVoiceSpeed;
        private TrackBar trackVol;
        private TrackBar trackVoicePitch;
        private Button bttnTalk;
        private ComboBox comboBoxVoices;
        private Button buttonVoice;
        private GroupBox groupBox1;
        private RadioButton radioHappy;
        private RadioButton radioAngry;
        private RadioButton radioNeutral;
        private RadioButton radioNoExpression;
        private Label lblVoice;
        private TableLayoutPanel tableLayoutPanel5;
        private RichTextBox richTextBoxLog;
        private Panel panel2;
        private Button buttonClear;
        private Panel panel3;
        private Button buttonStartRecording;
        private Button buttonStopRecording;
        private Panel panel4;
        private TextBox textLEDClr;
        private Button bttnChangeLED;
        private PictureBox pictureBoxSnapShot;
        private Button bttnSnapshot;
    }
}
