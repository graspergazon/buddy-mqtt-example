using System.Text;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using libBuddyControl;

namespace ControllerApp
{
    public partial class MainWindow : Form
    {
        private string MQTT_BROKER_ADDRESS = "86.80.141.60";
        //private string MQTT_BROKER_ADDRESS = "80.158.53.59"; //at

        private int MQTT_BROKER_PORT = 1883;


        private const string TOPIC_BUDDY_REQUEST = "buddy/request";
        private const string TOPIC_BUDDY_HEAD = "buddy/head";
        private const string TOPIC_BUDDY_BODY = "buddy/body";
        private const string TOPIC_BUDDY_VOICE = "buddy/voice";
        private const string TOPIC_BUDDY_RECORD = "buddy/record";


        private const string TOPIC_BUDDY_RESPONSE = "buddy/response";
        private const string TOPIC_BUDDY_DEBUG = "buddy/debug";
        private const string TOPIC_BUDDY_SNAPSHOT = "buddy/snapshot";






        private MqttClient _client;
        private List<BuddyFacialExpression> _expressions;
        private List<BuddyFacialEvent> _facialEvents;

        private int _expressionWhileTalkingIdx = 0;
        public MainWindow()
        {
            InitializeComponent();

            this.FormClosing += MainWindow_FormClosing;
            this.Load += MainWindow_Load;

            InitVoicePanel();
        }

        private void MainWindow_Load(object? sender, EventArgs e)
        {
            listBoxExpression.Items.Clear();
            listBoxFaceEvents.Items.Clear();


            _expressions = new List<BuddyFacialExpression>();
            _facialEvents = new List<BuddyFacialEvent>();

            //var expressionCount = Enum.GetNames(typeof(BuddyFacialExpression)).Length;
            var expressionValues = Enum.GetValues(typeof(BuddyFacialExpression));
            var faceEventValues = Enum.GetValues(typeof(BuddyFacialEvent));


            foreach (var item in expressionValues)
            {
                BuddyFacialExpression expression = (BuddyFacialExpression)item;
                listBoxExpression.Items.Add(expression.ToString());
                _expressions.Add(expression);

            }

            foreach (var item in faceEventValues)
            {
                BuddyFacialEvent expression = (BuddyFacialEvent)item;
                listBoxFaceEvents.Items.Add(expression.ToString());
                _facialEvents.Add(expression);

            }
        }

        private void InitVoicePanel()
        {
            comboBoxVoices.DataSource = Enum.GetValues(typeof(Voices));
            comboBoxVoices.SelectedIndex = 0;



            //trackVoicePitch.Minimum = 50; //between 50 and 200
            //trackVoicePitch.Maximum = 200;
            trackVoicePitch.SetRange(5, 20);
            trackVoicePitch.Value = 20;
            trackVoiceSpeed.SetRange(5, 40); // 50 and 400
            trackVoiceSpeed.Value = 10;
            trackVol.SetRange(0, 30);
            trackVol.Value = 10;
        }
        private void MainWindow_FormClosing(object? sender, FormClosingEventArgs e)
        {
            try
            {
                if (_client != null)
                {
                    //_client.Unsubscribe(new string[] { TOPIC_IN_RECORD });
                    //_client.MqttMsgPublishReceived -= Client_MqttMsgPublishReceived;
                    //_client.ConnectionClosed -= Client_ConnectionClosed;
                    _client.Disconnect();
                    _client = null;
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void connectToBrokerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConnectMqtt();
        }

        private void ConnectMqtt()
        {

            _client = new MqttClient(MQTT_BROKER_ADDRESS, MQTT_BROKER_PORT, false, null, null, MqttSslProtocols.None);

            _client.MqttMsgPublishReceived += _client_MqttMsgPublishReceived;
            _client.ConnectionClosed += _client_ConnectionClosed;

            string clientId = Guid.NewGuid().ToString();
            _client.Connect(clientId);
            string[] topics = new string[]
            {
                TOPIC_BUDDY_RESPONSE,
                TOPIC_BUDDY_DEBUG,
                TOPIC_BUDDY_SNAPSHOT

            };

            byte[] qosLevels = new byte[topics.Length];
            for (int i = 0; i < qosLevels.Length; i++)
            {
                qosLevels[i] = MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE;
            }


            _client.Subscribe(topics, qosLevels);

            if (_client.IsConnected)
            {
                toolStripStatusLabel1.Text = "Status: Connected";
            }
        }

        private void _client_ConnectionClosed(object sender, EventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(() => { toolStripStatusLabel1.Text = "Status: Disconnected"; });
            }
            else
            {
                toolStripStatusLabel1.Text = "Status: Disconnected";
            }

        }

        private void _client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            string topic = e.Topic;

            if (e.Topic == TOPIC_BUDDY_REQUEST)
            {
                string message = string.Empty;
                try
                {
                    message = Encoding.UTF8.GetString(e.Message);


                }
                catch (Exception ex)
                {

                }

                if (string.IsNullOrEmpty(message))
                {
                    return;
                }
                else
                    richTextBoxLog.AppendText(message + "\n");
            }
            else if (e.Topic == TOPIC_BUDDY_RESPONSE || e.Topic == TOPIC_BUDDY_DEBUG)
            {

                string message = string.Empty;
                try
                {
                    message = $"Response: {Encoding.UTF8.GetString(e.Message)}";


                }
                catch (Exception ex)
                {

                }

                if (string.IsNullOrEmpty(message))
                {
                    return;
                }
                else
                {
                    DisplayMessage(richTextBoxLog, message);

                    Console.WriteLine(message);

                }
            }
            else if (topic == TOPIC_BUDDY_SNAPSHOT)
            {
                try
                {
                    Bitmap bmp = null;
                    using (System.IO.MemoryStream ms = new System.IO.MemoryStream(e.Message))
                    {
                        bmp = new Bitmap(ms);
                    }

                    if (bmp != null)
                    {
                        if (this.InvokeRequired)
                        {
                            this.BeginInvoke(new Action(() => { pictureBoxSnapShot.Image = bmp; }));
                        }
                        else
                            pictureBoxSnapShot.Image = bmp;
                    }

                    //AddToLog(txtBoxLog, $"Video: Received snapshot at {DateTime.Now.ToLongTimeString()}");

                    //bmp.Save($"{DateTime.Now.ToString("yyyy_MM_dd__HH_mm_ss_ff")}.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                return;
            }

        }

        private void DisplayMessage(RichTextBox tbox, string message)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new Action(() => { tbox.AppendText(message + "\n"); }));
            }
            else
            {
                tbox.AppendText(message + "\n");
            }
        }

        private void SendMessage(string message, string topic)
        {

            if (_client == null)
            {
                return;
            }

            try
            {
                if (_client.IsConnected)
                {

                    _client.Publish(topic, System.Text.Encoding.UTF8.GetBytes(message));
                }
                else
                {
                    string clientId = Guid.NewGuid().ToString();
                    _client.Connect(clientId);
                    _client.Publish(topic, System.Text.Encoding.UTF8.GetBytes(message));
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
        }

        private void getStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_client != null)
            {
                if (_client.IsConnected)
                {
                    toolStripStatusLabel1.Text = "Status: Connected";
                    return;
                }
            }

            toolStripStatusLabel1.Text = "Status: Disconnected";

        }

        private void listBoxExpression_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idx = listBoxExpression.SelectedIndex;

            if (idx < 0)
                return;

            if (_client == null)
                return;

            BuddyFacialExpression selectedExpression = _expressions[idx];
            if (_client.IsConnected)
            {
                BuddyRequest request = new BuddyRequest
                {
                    RequestType = (int)BuddyRequestType.FacialExpression,
                    Payload = string.Empty,
                    ExpressionType = (int)selectedExpression
                };

                SendMessage(request.ToJsonString(), TOPIC_BUDDY_REQUEST);

            }
        }

        private void listBoxFaceEvents_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idx = listBoxFaceEvents.SelectedIndex;

            if (idx < 0)
                return;

            if (_client == null)
                return;

            BuddyFacialEvent selectedExpression = _facialEvents[idx];
            if (_client.IsConnected)
            {
                BuddyRequest request = new BuddyRequest
                {
                    RequestType = (int)BuddyRequestType.FacialEvent,
                    Payload = string.Empty,
                    ExpressionType = (int)selectedExpression
                };

                SendMessage(request.ToJsonString(), TOPIC_BUDDY_REQUEST);

            }
        }

        private void buttonVoice_Click(object sender, EventArgs e)
        {
            int vol = trackVol.Value * 10;
            int pitch = trackVoicePitch.Value * 10;
            int speed = trackVoiceSpeed.Value * 10;

            string voice = comboBoxVoices.SelectedValue.ToString();

            VoiceRequest request = new VoiceRequest { RequestType = 2, Pitch = pitch, Speed = speed, Voice = voice, Volume = vol };

            SendMessage(request.ToJsonString(), TOPIC_BUDDY_VOICE);

            lblVoice.Text = $"Voice: {voice}, Speed: {speed}, Pitch: {pitch}, Volume: {vol}";
        }

        private void bttnTalk_Click(object sender, EventArgs e)
        {
            string txt = richTextBoxSpeech.Text;

            if (string.IsNullOrEmpty(txt))
                return;

            BuddyRequest request = new BuddyRequest(BuddyRequestType.Speech, txt);

            request.ExpressionType = _expressionWhileTalkingIdx;

            SendMessage(request.ToJsonString(), TOPIC_BUDDY_REQUEST);
        }

        private void buttonShutUp_Click(object sender, EventArgs e)
        {
            BuddyRequest request = new BuddyRequest(BuddyRequestType.ClearSpeech, "Stop");

            request.ExpressionType = _expressionWhileTalkingIdx;

            SendMessage(request.ToJsonString(), TOPIC_BUDDY_REQUEST);
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            richTextBoxLog.Clear();
        }

        private void buttonStartRecording_Click(object sender, EventArgs e)
        {
            //BuddyRequest request = new BuddyRequest(BuddyRequestType.StartRecording, "");
            //SendMessage(request.ToJsonString(), TOPIC_BUDDY_REQUEST);


            RecordingRequest request = new RecordingRequest(RecordingRequestType.Start, new Size(1280, 720), 30);

            SendMessage(request.ToJsonString(), TOPIC_BUDDY_RECORD);


        }

        private void buttonStopRecording_Click(object sender, EventArgs e)
        {
            //BuddyRequest request = new BuddyRequest(BuddyRequestType.StopRecording, "");
            //SendMessage(request.ToJsonString(), TOPIC_BUDDY_REQUEST);

            RecordingRequest request = new RecordingRequest(RecordingRequestType.Stop);

            SendMessage(request.ToJsonString(), TOPIC_BUDDY_RECORD);
        }

        private void bttnChangeLED_Click(object sender, EventArgs e)
        {
            string clr = textLEDClr.Text;

            BuddyRequest request = new BuddyRequest(BuddyRequestType.LEDCOLOR, clr);
            SendMessage(request.ToJsonString(), TOPIC_BUDDY_REQUEST);
        }

        private void bttnSnapshot_Click(object sender, EventArgs e)
        {
            RecordingRequest request = new RecordingRequest(RecordingRequestType.SnapShot);

            SendMessage(request.ToJsonString(), TOPIC_BUDDY_RECORD);
        }
    }
}
