﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace libBuddyControl
{

    public class BaseRequest
    {
        public BaseRequest()
        {
            LocalTimestamp = BaseRequest.GetTimestampString();
        }

        public int RequestType { get; set; }

        public string LocalTimestamp { get; set; }

        public virtual string ToJsonString()
        {
            string json = JsonSerializer.Serialize(this);
            return json;
        }

        public static string GetTimestampString()
        {
            return DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss.fff");//"2022-01-06T21:30:10"
        }
    }

    public class VoiceRequest : BaseRequest
    {
        public string Voice { get; set; }

        public int Volume { get; set; }

        public int Speed { get; set; }

        public int Pitch { get; set; }


        public override string ToJsonString()
        {
            string json = JsonSerializer.Serialize(this);
            return json;
        }

    }

    public class RecordingRequest : BaseRequest
    {
        public int Width { get; set; }

        public int Height { get; set; }

        public int Fps { get; set; }

        public int Bitrate { get; set; }

        public int EncoderType { get; set; }

        public RecordingRequest(RecordingRequestType requestType, Size resolution, int fps)
        {
            RequestType = (int)requestType;


            Width = resolution.Width;
            Height = resolution.Height;
            Fps = fps;
            Bitrate = 10000000;

            EncoderType = (int)VideoEncoding.H264;
        }

        public RecordingRequest(RecordingRequestType requestType) : this(requestType, new Size(1280, 720), 30)
        {

        }

        public override string ToJsonString()
        {
            string json = JsonSerializer.Serialize(this);
            return json;
        }
    }


    public class MovementRequest : BaseRequest
    {
        public float Speed { get; set; }

        public float Angle { get; set; }


        public override string ToJsonString()
        {
            string json = JsonSerializer.Serialize(this);
            return json;
        }
    }

    public class BuddyRequest : BaseRequest
    {

        public int ExpressionType { get; set; }

        public string Payload { get; set; }


        public BuddyRequest()
        {

        }

        public BuddyRequest(BuddyRequestType requestType, string paload)
        {
            RequestType = (int)requestType;
            Payload = paload;
        }

        public static BuddyRequest FromJsonString(string jsonString)
        {
            return JsonSerializer.Deserialize<BuddyRequest>(jsonString);
        }
        public override string ToJsonString()
        {
            string json = JsonSerializer.Serialize(this);
            return json;
        }

    }

    public class HeadRequest : MovementRequest
    {
        public int Status { get; set; }


        public HeadRequest()
        {
        }

        public HeadRequest(HeadRequestType requestType, float speed, float angle, int status)
        {
            RequestType = (int)requestType;
            Speed = speed;
            Angle = angle;
            Status = status;
        }

        public static HeadRequest FromJsonString(string jsonString)
        {
            return JsonSerializer.Deserialize<HeadRequest>(jsonString);
        }

        public override string ToJsonString()
        {
            string json = JsonSerializer.Serialize(this);
            return json;
        }
    }

    public class BodyRequest : MovementRequest
    {
        public int LeftStatus { get; set; }
        public int RightStatus { get; set; }


        public BodyRequest()
        {
        }

        public BodyRequest(BodyRequestType requestType, float speed, float angle, int leftStatus, int rightStatus)
        {
            RequestType = (int)requestType;
            Speed = speed;
            Angle = angle;
            LeftStatus = leftStatus;
            RightStatus = rightStatus;

        }

        public static BodyRequest FromJsonString(string jsonString)
        {
            return JsonSerializer.Deserialize<BodyRequest>(jsonString);
        }

        public override string ToJsonString()
        {
            string json = JsonSerializer.Serialize(this);
            return json;
        }

    }

    public enum BuddyRequestType { Debug = 0, Speech = 2, FacialExpression = 4, FacialEvent = 8, ClearSpeech = 3, StartRecording = 5, StopRecording = 6, LEDCOLOR = 7 }

    public enum RecordingRequestType { SnapShot = 0, Start = 1, Stop = 2 }


    public enum HeadRequestType
    {
        EnableYes = 2, EnableNo = 4, GetYesStatus = 8, GetNoStatus = 16, StopNo = 32, StopYes = 64,
        GetYesPos = 128, GetNoPos = 256, MoveYes = 512, MoveNo = 1024, MoveYesStraight = 2048, MoveNoStraight = 4096
    }

    public enum BodyRequestType
    {
        EnableWheels = 2, Rotate = 4, Move = 8, Stop = 16,
    }

    public enum VideoEncoding
    {
        H264 = 0, MJPEG = 1
    }


    public enum BuddyFacialExpression
    {
        NONE = 0,
        NEUTRAL = 1,
        ANGRY = 2,
        GRUMPY = 3,
        HAPPY = 4,
        LISTENING = 5,
        SAD = 6,
        SCARED = 7,
        SICK = 8,
        SURPRISED = 9,
        THINKING = 10,
        TIRED = 11,
        LOVE = 12
    }

    public enum BuddyFacialEvent
    {
        SMILE = 13,
        YAWN = 14,
        SURPRISED = 9,
        BLINK_RIGHT_EYE = 16,
        BLINK_LEFT_EYE = 15,
        BLINK_EYES = 17,
        TEASE = 18,
        SUSPICIOUS = 19,
        DOUBTFUL = 20,
        WHAT = 21,
        GROWLING = 22,
        WHISTLE = 23,
        CLOSE_EYES = 24,
        OPEN_EYES = 25,
        CLOSE_LEFT_EYE = 28,
        OPEN_LEFT_EYE = 29,
        CLOSE_RIGHT_EYE = 26,
        OPEN_RIGHT_EYE = 27,
        FALL_ASLEEP = 30,
        AWAKE = 31,
        RELATIVE_EVENT = 32
    }

    public enum Voices { Guus, Alex, Kate }

}



