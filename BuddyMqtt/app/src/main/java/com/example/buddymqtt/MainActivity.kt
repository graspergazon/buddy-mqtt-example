package com.example.buddymqtt

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.RemoteException
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bfr.buddy.speech.shared.ITTSCallback
import com.bfr.buddysdk.BuddyActivity
import com.bfr.buddysdk.BuddySDK
import com.bfr.buddysdk.BuddySDK.USB
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended
import org.eclipse.paho.client.mqttv3.MqttMessage
import java.text.SimpleDateFormat
import java.util.Date
import java.util.LinkedList
import java.util.Queue

const val MQTT_SERVER = "tcp://86.80.141.60:1883"
const val MQTT_TOPIC_REQUEST = "buddy/request"
const val MQTT_TOPIC_HEAD = "buddy/head"
const val MQTT_TOPIC_BODY = "buddy/body"
const val MQTT_TOPIC_VOICE = "buddy/voice"
const val MQTT_TOPIC_RECORD = "buddy/record"


const val MQTT_TOPIC_RESPONSE = "buddy/response"

const val MQTT_TOPIC_SNAPSHOT = "buddy/snapshot"


const val DEBUGTAG = "BuddyMQTT"


class MainActivity :  BuddyActivity() {

    private lateinit var _mqttClient : MqttHelper
    private var _isSDKInitialized : Boolean = false
    private lateinit var _speechController : TextToSpeechHelper
    private val _speechQueue : Queue<SpeechMessage> = LinkedList()
    private lateinit var _videoRecorder : Recorder

    private var isRecording : Boolean = false
    var _deviceID  : String = "UNKNOWN"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (!hasRequiredPermissions()) {
            ActivityCompat.requestPermissions(
                this, REQUIRED_PERMISSIONS, 0
            )
        }

        _deviceID = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)

        _videoRecorder = Recorder(this)
        _videoRecorder.recordingStartedCallback = { result: String? -> signalRecordingStarted(result!!) }
        _videoRecorder.snapshotTakenCallback = { result: ByteArray? -> tryToSendPictureBuffer(result!!) }


        val bttn: Button = findViewById<Button>(R.id.button)

        bttn.text = "Start"
        //bttn.visibility = View.INVISIBLE

        isRecording = false;
        bttn.setOnClickListener()
        {
            val hasInternet = Utils.checkForInternet(this);

            if(!hasInternet){
                Log.d(DEBUGTAG, "No internet available");


            }
            else
            {

                /*
                _mqttClient = MqttHelper(this, MQTT_SERVER)
                setMqttCallBack()

                if(!_mqttClient.isConnected())
                    _mqttClient.connect()*/
            }


            if(!isRecording){

                //_videoRecorder.StartRecording()
                _videoRecorder.StartImageCapture()
                isRecording = true
                ChangeLedColor()
                bttn.text = "Stop"


            }
            else{
                //_videoRecorder.StopRecording()
                _videoRecorder.StopImageCapture()
                isRecording = false
                ChangeLedColor()
                bttn.text = "Start"
            }
        }

    }

    private fun signalRecordingStarted(info:String){
        publishResponse("Recording started callback $info", 6)

        //_videoRecorder.StopRecording()

    }

    private fun tryToSendPictureBuffer(buffer:ByteArray){
        //publishResponse("Recording started callback $info", 6)
        if(_mqttClient!=null){

            if(_mqttClient.isConnected())
                _mqttClient.publishBytes(MQTT_TOPIC_SNAPSHOT, buffer)
            else{
                Log.d(DEBUGTAG, "Could send data over MQTT, client not connected");
            }
        }

    }

    fun ChangeLedColor(){

        if(_isSDKInitialized){
            if(isRecording)
                USB.updateLedColor(2, "#00FF00", null)
            else
                if(_isSDKInitialized){
                    USB.updateLedColor(2, "#00FFFF", null)
                }
        }
    }

    fun requestSnapshot(){
        if(!isRecording)
        {
            _videoRecorder.requestSnapshot()
        }
    }

    fun startRecording(encoderType:Int, bitrate:Int){

        if(!isRecording)
        {
            _videoRecorder.StartRecording()
            isRecording = true


            Toast.makeText(this, "Recording started", Toast.LENGTH_SHORT).show()
            ChangeLedColor()
            publishResponse("Recording started", 5)

        }
        else{
            publishResponse("Device was already recording", 5)

        }
    }

    fun stopRecording(){
        if(isRecording){
            _videoRecorder.StopRecording()
            isRecording = false
            Toast.makeText(this, "Recording stopped", Toast.LENGTH_SHORT).show()
            ChangeLedColor()
            publishResponse("Recording stopped", 6)
        }
        else{
            publishResponse("No recording to stop", 6)
        }


    }

    fun InitMqttClient(){
        val hasInternet = Utils.checkForInternet(this);

        if(!hasInternet){
            Log.d(DEBUGTAG, "No internet available");
            return;

        }
        else
        {
            if(this::_mqttClient.isInitialized){
                Log.d(DEBUGTAG, "_mqttClient already init");
            }


            _mqttClient = MqttHelper(this, MQTT_SERVER)
            setMqttCallBack()

            if(!_mqttClient.isConnected())
                _mqttClient.connect()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        //Log.d(DEBUGTAG, "OnDestroy Called");

        _speechQueue.clear()

        if(this::_mqttClient.isInitialized){
            if(_mqttClient!=null)
            {
                _mqttClient.destroy()

            }
        }

    }
    override fun onStop() {
        super.onStop()

        _speechQueue.clear()

        //Log.i(DEBUGTAG, "OnStop Called");
    }


    override fun onResume() {
        super.onResume()

        InitMqttClient()
    }

    override fun onRestart() {
        super.onRestart()

        _speechQueue.clear()

        Log.d(DEBUGTAG, "OnRestart Called");

        if(this::_mqttClient.isInitialized)
        {
            if(_mqttClient!=null)
            {
                Log.d(DEBUGTAG, "OnRestart: _mqttClient is not null");
                _mqttClient.destroy()

            }
            else
            {
                Log.d(DEBUGTAG, "OnRestart: _mqttClient is null");
            }
        }
        else
        {

        }




    }
    private fun handleRecordingRequest( request: RecordingRequest )
    {
        if(!isRecording)
        {
            _videoRecorder.changeRecordingSettings(request.Width, request.Height, request.Fps)
        }


        if(request.RequestType==0) //snapshot
        {
            requestSnapshot()
        }
        else if (request.RequestType==1){ //start
            startRecording(request.EncoderType, request.Bitrate);
        }
        else if (request.RequestType==2){ //stop

            stopRecording();

        }
    }

    private fun handleBuddyRequest( mqqtMessage: String )
    {


        try {
            val request = SerializationHelper.buddyRequestfromString(mqqtMessage)

            val timestamp = SerializationHelper.ParseTimeString(request.LocalTimestamp)



            when(request.RequestType){
                2->{ //speech request

                    if(!_isSDKInitialized){

                        Toast.makeText(this, request.Payload, Toast.LENGTH_SHORT).show()
                        return
                    }

                    if(_speechController.isTalking()){
                        _speechQueue.add(SpeechMessage(request.Payload, request.ExpressionType))
                    }
                    else{
                        _speechController.talk(request.Payload, request.ExpressionType)
                    }

                }
                3->{
                    //clear speech
                    //empty queue
                    _speechQueue.clear()
                    //stop speech
                    _speechController.stopTalking()


                }
                4->{
                    buddySetMood(request.ExpressionType)
                }
                5->{
                    startRecording(0, -1)
                }
                6->{
                    stopRecording()
                }
                7->{
                    USB.updateLedColor(2, request.Payload, null)
                }
                8->{
                    buddySetFacialEvent(request.ExpressionType)
                }
                else->{


                }
            }


        }
        catch (e : Throwable)
        {
            Log.d(DEBUGTAG, "Exception " + e.toString());
        }
    }

    private fun buddySetMood(expressionType : Int){

        //var expression = FacialExpression.values().first()
        BuddySDK.UI.setMood(BuddyHelper.expressionFromInt(expressionType))
    }

    private fun buddySetFacialEvent(expressionType : Int){

        //var expression = FacialExpression.values().first()
        BuddySDK.UI.playFacialEvent(BuddyHelper.facialEventFromInt(expressionType))
    }

    override fun onSDKReady() {
        super.onSDKReady()

        _isSDKInitialized = true
        BuddySDK.UI.setViewAsFace(findViewById(R.id.buddyMqttLayout))



        _speechController = TextToSpeechHelper()
        _speechController.speechReadyCallback = { result: String? -> speechSuccess(result!!) }
        _speechController.speechOtherCallback = { result: String? -> speechOther(result!!) }
        _speechController.setBuddyVoice()
    }

    @Throws(RemoteException::class)
    fun speechSuccess(success: String) : Unit {

        var speechMessage = _speechQueue.poll()

        if(speechMessage!=null)
        {
            _speechController.talk(speechMessage.text, speechMessage.expressionType)
        }

        publishResponse(success, 2)

    }

    @Throws(RemoteException::class)
    fun speechOther(success: String) : Unit {



        publishResponse(success, 2)
    }

    private val ttsCallback: ITTSCallback =
        object : ITTSCallback.Stub()
        {
            override fun onSuccess(p0: String?) {
                TODO("Not yet implemented")
            }

            override fun onError(p0: String?) {
                TODO("Not yet implemented")
            }

            override fun onPause() {
                TODO("Not yet implemented")
            }

            override fun onResume() {
                TODO("Not yet implemented")
            }
        }

    fun publishResponse(message: String, messageType:Int)
    {
        val timestamp = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(Date())
        var response = BuddyResponse( messageType,message, _deviceID, 0,timestamp )
        var json = SerializationHelper.toString(response)
        publishResponse(json)
    }

    fun publishResponse(jsonMessage: String){

        if(_mqttClient!=null){

            if(_mqttClient.isConnected())
                _mqttClient.publish(MQTT_TOPIC_RESPONSE, jsonMessage)
            else{
                Log.d(DEBUGTAG, "Could send data over MQTT, client not connected");
            }
        }
        else{
            Log.d(DEBUGTAG, "Could send data over MQTT, client not initialized (null)");

        }
    }


    private fun setMqttCallBack() {

            _mqttClient.setCallback(object : MqttCallbackExtended {
            override fun connectionLost(cause: Throwable?) {
                //txtView.text = "Connetion lost"
                Log.d(DEBUGTAG, "Mqtt client connection lost");

            }

            @Throws(Exception::class)
            override fun messageArrived(topic: String?, message: MqttMessage?) {


                if(topic == MQTT_TOPIC_REQUEST){
                    handleBuddyRequest(message.toString())

                    Log.d(DEBUGTAG, "Received: " + message.toString());

                }
                else if( topic== MQTT_TOPIC_RECORD){
                    val request = SerializationHelper.recordingRequestfromString(message.toString())
                    handleRecordingRequest(request)

                }
                else if( topic== MQTT_TOPIC_HEAD){
                    //handleHeadMessage(message.toString())

                }
                else if( topic== MQTT_TOPIC_BODY){
                    //handleBodyMessage(message.toString())

                }
                else if( topic== MQTT_TOPIC_VOICE){
                    //handleBodyMessage(message.toString())

                    if(_isSDKInitialized){
                        if(_speechController!=null)
                        {
                            val voiceRequest = SerializationHelper.voiceRequestfromString(message.toString())
                            if(voiceRequest.RequestType<=8){
                                _speechController.UpdateVoice(voiceRequest)
                            }
                            else{
                                //debug
                            }
                        }
                    }
                }
                else{
                    Log.d(DEBUGTAG, "Unknown mqtt message type");

                }


            }

            override fun deliveryComplete(token: IMqttDeliveryToken?) {

            }

            override fun connectComplete(reconnect: Boolean, serverURI: String?) {

                _mqttClient.subscribeMulti(arrayOf(
                    MQTT_TOPIC_REQUEST,
                    MQTT_TOPIC_HEAD,
                    MQTT_TOPIC_BODY,
                    MQTT_TOPIC_VOICE,
                    MQTT_TOPIC_RECORD),
                    intArrayOf(1,1,1,1,1))

                //txtView.text = "connected"

                publishResponse("Buddy with id $_deviceID connected", 0 )

            }

        })
    }

    private fun hasRequiredPermissions(): Boolean {
        return REQUIRED_PERMISSIONS.all {
            ContextCompat.checkSelfPermission(
                applicationContext,
                it
            ) == PackageManager.PERMISSION_GRANTED
        }
    }

    companion object {
        private const val REQUEST_CODE_PERMISSIONS = 10
        private val REQUIRED_PERMISSIONS =
            mutableListOf (
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
            ).apply {
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
                    add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                }
            }.toTypedArray()
    }
}