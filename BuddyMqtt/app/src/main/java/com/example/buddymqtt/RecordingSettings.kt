package com.example.buddymqtt

import kotlinx.serialization.Serializable

class RecordingSettings {
}

@Serializable
data class RecordingRequest(val RequestType: Int, val LocalTimestamp:String,
                            val Width:Int, val Height:Int, val Fps: Int,
                            val Bitrate: Int, val EncoderType: Int)
