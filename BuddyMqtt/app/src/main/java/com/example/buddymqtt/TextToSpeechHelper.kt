package com.example.buddymqtt

import com.bfr.buddy.speech.shared.ITTSCallback
import com.bfr.buddy.ui.shared.LabialExpression
import com.bfr.buddysdk.BuddySDK

class TextToSpeechHelper: ITTSCallback.Stub() {

    var speechReadyCallback: ((p0: String?)->Unit)? = null
    var speechOtherCallback: ((p0: String?)->Unit)? = null

    fun talk(stuffToSay: String, expression: LabialExpression)
    {
        BuddySDK.Speech.startSpeaking(stuffToSay, expression,  this)
    }

    fun setBuddyVoice(){
        BuddySDK.Speech.setSpeakerVoice("Guus")
        BuddySDK.Speech.setSpeakerPitch(300)
        BuddySDK.Speech.speakerSpeed = 110

    }

    fun isTalking(): Boolean
    {
        return  BuddySDK.Speech.isSpeaking
    }
    fun talk(stuffToSay: String, expression: Int)
    {
        var speechExpression = LabialExpression.NO_EXPRESSION;
        when(expression){
            1->{
                speechExpression = LabialExpression.SPEAK_NEUTRAL;
            }
            2->{
                speechExpression = LabialExpression.SPEAK_ANGRY;
            }
            4->{
                speechExpression = LabialExpression.SPEAK_HAPPY;
            }
            else->{
                speechExpression = LabialExpression.NO_EXPRESSION;

            }
        }

        BuddySDK.Speech.startSpeaking(stuffToSay, speechExpression,  this)
    }

    fun UpdateVoice(voiceRequest: VoiceRequest)
    {
        val requestType = voiceRequest.RequestType

        BuddySDK.Speech.setSpeakerVoice(voiceRequest.Voice)

        BuddySDK.Speech.speakerPitch = voiceRequest.Pitch

        BuddySDK.Speech.speakerSpeed = voiceRequest.Speed

        BuddySDK.Speech.speakerVolume = voiceRequest.Volume

    }
    fun stopTalking()
    {
        BuddySDK.Speech.stopSpeaking()

    }

    override fun onSuccess(p0: String?) {

        speechReadyCallback?.invoke(p0)
    }

    override fun onPause() {
        speechOtherCallback?.invoke("Pause")
    }

    override fun onResume() {
        speechOtherCallback?.invoke("Resume")

    }

    override fun onError(p0: String?) {
        speechOtherCallback?.invoke("Error")

    }

}