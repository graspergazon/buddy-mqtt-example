package com.example.buddymqtt

import android.os.RemoteException
import com.bfr.buddy.ui.shared.FacialEvent
import com.bfr.buddy.ui.shared.FacialExpression
import com.bfr.buddy.usb.shared.IUsbCommadRsp
import com.bfr.buddysdk.BuddySDK


class BuddyHelper {
    companion object {
        fun expressionFromInt(expressiontType: Int) : FacialExpression
        {
            var options = FacialExpression.values()

            for (item in options) {

                if(item.ordinal == expressiontType)
                    return  item
            }

            return  FacialExpression.NONE
        }

        fun facialEventFromInt(expressiontType: Int) : FacialEvent
        {
            var options = FacialEvent.values()

            for (item in options) {

                if(item.value == expressiontType)
                    return  item
            }

            return  FacialEvent.SMILE
        }

        fun getHeadYesStatus() : String
        {
            //USB.buddySayNo()
            return BuddySDK.Actuators.getYesStatus()
        }

        fun getHeadStatus()
        {
            var yStatus = BuddySDK.Actuators.getYesStatus()
            var yPos = BuddySDK.Actuators.getYesPosition()
            var noStatus = BuddySDK.Actuators.getNoStatus()
            var noPos = BuddySDK.Actuators.getNoPosition()
        }

        fun GetSoundDirection():Float
        {
            return BuddySDK.Sensors.Microphone().soundLocalisation;
        }


        /*fun getHeadRequestType(intNr : Int): HeadRequestType{

            return intNr as HeadRequestType
        }*/

    }
}

class MoveController: IUsbCommadRsp.Stub(), IUsbCommadRsp {
    var successCallback: ((p0: String?)->Unit)? = null
    var failCallback: ((p0: String?)->Unit)? = null


    fun enableWheels(leftState: Int, rightState: Int){
        BuddySDK.USB.enableWheels(leftState,rightState, this)
    }

    fun moveBuddy(speed: Float, distance: Float){
        BuddySDK.USB.moveBuddy(speed,distance ,this )
    }

    fun rotateBuddy(speed: Float, angle: Float){
        BuddySDK.USB.rotateBuddy(speed,angle ,this )
    }

    fun stopMotors(){
        BuddySDK.USB.emergencyStopMotors(this )
    }

    @Throws(RemoteException::class)
    override fun onSuccess(p0: String?) {

        successCallback?.invoke(p0)
    }
    @Throws(RemoteException::class)
    override fun onFailed(p0: String?) {

        failCallback?.invoke(p0)

    }
}


class BuddyController: IUsbCommadRsp.Stub(), IUsbCommadRsp {

    var successCallback: ((p0: String?)->Unit)? = null
    var failCallback: ((p0: String?)->Unit)? = null


    fun setCallbacks( onSucc: (p0: String?)->Unit, onFail: (p0: String?)->Unit) {
        //mqttClient.setCallback(callback)
        successCallback = onSucc
        failCallback = onFail
    }

    fun enableYes(state: Int){
        BuddySDK.USB.enableYesMove(state,this)
    }

    fun enableNo(state: Int){
        BuddySDK.USB.enableNoMove(state,this)
    }

    fun getYesStatus():String{
        return BuddySDK.Actuators.getYesStatus()
    }

    fun getNoStatus():String{
        return BuddySDK.Actuators.noStatus
    }

    fun getYesPos():Int{
        return BuddySDK.Actuators.yesPosition
    }

    fun getNoPos():Int{
        return BuddySDK.Actuators.noPosition
    }

    fun moveYesStraight(speed: Float){
        BuddySDK.USB.buddySayYesStraight(speed,this )
    }
    fun moveNoStraight(speed: Float){
        BuddySDK.USB.buddySayNoStraight(speed, this)
    }

    fun moveYes(speed: Float, angle: Float){
        BuddySDK.USB.buddySayYes(speed,angle ,this )
    }
    fun moveNo(speed: Float, angle: Float){
        BuddySDK.USB.buddySayNo(speed, angle, this)
    }

    fun stopNo(speed: Float, angle: Float){
        BuddySDK.USB.buddyStopNoMove( this)
    }

    fun stopYes(speed: Float, angle: Float){
        BuddySDK.USB.buddyStopYesMove( this)
    }

    fun enableWheels(leftState: Int, rightState: Int){
        BuddySDK.USB.enableWheels(leftState,rightState, this)
    }

    fun moveBuddy(speed: Float, distance: Float){
        BuddySDK.USB.moveBuddy(speed,distance ,this )
    }

    fun rotateBuddy(speed: Float, angle: Float){
        BuddySDK.USB.rotateBuddy(speed,angle ,this )
    }

    fun stopMotors(){
        BuddySDK.USB.emergencyStopMotors(this )
    }

    @Throws(RemoteException::class)
    override fun onSuccess(p0: String?) {
        //TODO("Not yet implemented")

        // if(osucces!=null)
        successCallback?.invoke(p0)
    }
    @Throws(RemoteException::class)
    override fun onFailed(p0: String?) {
        //TODO("Not yet implemented")
        failCallback?.invoke(p0)

    }


}

enum class HeadRequestType(val typeNr: Int) {
    EnableYes (2),
    EnableNo ( 4),
    GetYesStatus ( 8),
    GetNoStatus (16),
    StopNo ( 32),
    StopYes ( 64) ,
    GetYesPos ( 128),
    GetNoPos ( 256),
    MoveYes ( 512),
    MoveNo ( 1024),
    MoveYesStraight ( 2048),
    MoveNoStraight ( 4096);

    companion object {
        fun fromInt(value: Int) = HeadRequestType.values().first { it.typeNr == value }
    }
}

enum class BodyRequestType(val typeNr: Int) {
    EnableWheels (2),
    Rotate ( 4),
    Move ( 8),
    Stop (16);


    companion object {
        fun fromInt(value: Int) = BodyRequestType.values().first { it.typeNr == value }
    }
}