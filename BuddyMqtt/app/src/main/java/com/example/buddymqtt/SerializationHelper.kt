package com.example.buddymqtt

import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.time.LocalDateTime

class SerializationHelper {

    companion object {
        fun toString(buddyRequest: BuddyRequest) : String
        {
            val jsonString = Json.encodeToString(buddyRequest)
            return jsonString
        }

        fun toString(buddyResponse: BuddyResponse) : String
        {
            val jsonString = Json.encodeToString(buddyResponse)
            return jsonString
        }

        fun buddyRequestfromString(jsonString: String) : BuddyRequest
        {
            val obj = Json.decodeFromString<BuddyRequest>(jsonString)
            return obj
        }

        fun recordingRequestfromString(jsonString: String) : RecordingRequest
        {
            val obj = Json.decodeFromString<RecordingRequest>(jsonString)
            return obj
        }

        fun headRequestFromString(jsonString: String) : HeadRequest
        {
            val obj = Json.decodeFromString<HeadRequest>(jsonString)
            return obj
        }
        fun bodyRequestfromString(jsonString: String) : BodyRequest
        {
            val obj = Json.decodeFromString<BodyRequest>(jsonString)
            return obj
        }

        fun voiceRequestfromString(jsonString: String) : VoiceRequest
        {
            val obj = Json.decodeFromString<VoiceRequest>(jsonString)
            return obj
        }

        fun ParseTimeString(timeString:String) : LocalDateTime{
            val localDateTime = LocalDateTime.parse(timeString)

            return  localDateTime
        }

    }
}

@Serializable
data class BuddyRequest(val RequestType: Int, val Payload:String, val ExpressionType:Int, val LocalTimestamp:String )

@Serializable
data class BuddyResponse(val RequestType: Int, val Status:String, val DeviceID:String, val HeadValue:Int, val LocalTimestamp:String)

@Serializable
data class HeadRequest(val RequestType: Int, val LocalTimestamp:String, val Speed:Float, val Angle:Float, val Status: Int)

@Serializable
data class BodyRequest(val RequestType: Int, val LocalTimestamp:String, val Speed:Float, val Angle:Float, val LeftStatus: Int, val RightStatus: Int)

@Serializable
data class VoiceRequest(val RequestType: Int, val LocalTimestamp:String, val Voice:String, val Volume:Int, val Speed: Int, val Pitch: Int)

enum class VoiceUpdateMode(val value:Int) {   VOICE(1), VOLUME(2), SPEED(4), PITCH(8) }

data class SpeechMessage ( val text : String, val expressionType:Int)
