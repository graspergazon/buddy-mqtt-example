package com.example.buddymqtt

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.ImageFormat
import android.graphics.SurfaceTexture
import android.os.Handler
import android.os.HandlerThread

import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraCaptureSession
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraDevice
import android.hardware.camera2.CameraManager
import android.hardware.camera2.CameraMetadata
import android.hardware.camera2.CaptureRequest
import android.hardware.camera2.TotalCaptureResult
import android.hardware.camera2.params.StreamConfigurationMap
import android.media.ImageReader
import android.media.MediaRecorder
import android.os.Environment
import android.util.Log

import android.util.Range
import android.util.Size
import android.widget.Toast
import java.io.File
import java.io.FileOutputStream
import java.io.FileWriter
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Objects
import kotlin.math.abs

class Recorder (ctx: Context){

    private lateinit var backgroundThread: HandlerThread
    private lateinit var backgroundThreadHandler: Handler
    private lateinit var imageReader: ImageReader

    private lateinit var cameraDevice: CameraDevice

    private lateinit var captureSession: CameraCaptureSession
    private lateinit var captureBuilder: CaptureRequest.Builder

    private var isRecording = false

    private var recordingWidth: Int = 1280
    private var recordingHeight: Int = 720
    private var recordingFps: Int = 30
    private var recordingBitrate: Int = 10000000

    private var deviceId: String = ""

    private var characteristics: CameraCharacteristics? = null

    private var size: Size = Size(0, 0)
    private var lensRotationInRadians: Float = 0f
    private var lastNotifiedOrientation = 0.0

    private var context: Context? = null;
    private var useImageReader = false
    private var hasWrittenImage = false
    private var frameCount: Long = 0

    private var takeSnapshot = false


    private lateinit var mjpegFileStream: OutputStream
    private lateinit var tsFileStream: FileWriter

    private val cameraManager by lazy {
        context!!.getSystemService(Context.CAMERA_SERVICE) as CameraManager
    }

    private val mediaRecorder by lazy {
        MediaRecorder()
    }
    private lateinit var currentVideoFilePath: String

    var recordingStartedCallback: ((p0: String?)->Unit)? = null
    var snapshotTakenCallback: ((p0: ByteArray?)->Unit)? = null

    init
    {
        context = ctx
    }

    public fun changeRecordingSettings(width:Int, height:Int, fps:Int){

        recordingWidth = width
        recordingHeight = height
        recordingFps = fps

    }

    public fun requestSnapshot(){

        //recordingWidth = width
       // recordingHeight = height
        takeSnapshot = true

        useImageReader = true;
        startBackgroundThread()

        findCamera()

        connectCamera()

    }

    private fun createMJPGFile()
    {
        val timestamp = SimpleDateFormat("yyyyMMdd_HHmss").format(Date())
        val fnameVid = "Video_$timestamp.mjpg"
        val fnameTs = "Video_$timestamp.txt"
        val videoFile = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES)!!,fnameVid)
        val tsFile = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES)!!,fnameTs)

        mjpegFileStream = FileOutputStream(videoFile)
        tsFileStream = FileWriter(tsFile)
        frameCount = 0


    }

    private  fun closeMJPEGFile(){
        mjpegFileStream.close()
        tsFileStream.close()
    }

    public fun StartRecording(){
        takeSnapshot = false
        useImageReader = false

        startBackgroundThread()
        findCamera()
        connectCamera()
    }

    public fun StopRecording(){
        isRecording = false
        stopMediaRecorder()
        safelyCloseDevice()
        stopBackgroundThread()
    }


    public fun StartImageCapture(){

        useImageReader = true;
        startBackgroundThread()

        findCamera()
        createMJPGFile()
        connectCamera()

    }

    public fun StopImageCapture(){

        isRecording = false;
        imageReader.close()
        safelyCloseDevice()
        closeMJPEGFile()
        stopBackgroundThread()
        useImageReader = false;


    }

    private fun startBackgroundThread(){
        backgroundThread = HandlerThread("vvcamera background thread").also { it.start() }
        backgroundThreadHandler = Handler(backgroundThread.looper)
    }

    private fun stopBackgroundThread(){
        backgroundThread.quitSafely()
        try {
            backgroundThread.join()
        }catch (e: InterruptedException){
            Log.e(DEBUGTAG, e.toString())
        }
    }

    private fun stopMediaRecorder(){
        mediaRecorder.apply {
            try{
                Log.d(DEBUGTAG, "Stopping media recorder")
                stop()
                reset()}
            catch (e: IllegalStateException){
                Log.e(DEBUGTAG,e.toString())
            }
        }
    }

    @Synchronized
    private fun safelyCloseDevice() {

        if(this::captureSession.isInitialized)
        {
            captureSession.close()
        }

        if(this::cameraDevice.isInitialized)
        {
            cameraDevice.close()
        }

    }

    private fun createFileName():String{
        val timestamp = SimpleDateFormat("yyyyMMdd_HHmss").format(Date())
        return "Video_$timestamp.mp4"
    }

    private fun createVideoFile(): File {
        //val videoFile = File(this.filesDir, createFileName())
        //val videoFile = File(this.getExternalFilesDir(null)!!, createFileName())
        val videoFile = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES)!!, createFileName())

        //this.getExternalFilesDir(null)!!
        currentVideoFilePath = videoFile.absolutePath
        //https://commonsware.com/Jetpack/pages/chap-files-001.html

        return videoFile
    }

    private fun createImageReader(){
        imageReader = ImageReader.newInstance(recordingWidth, recordingHeight, ImageFormat.JPEG, 1)

        /*
        imageReader.setOnImageAvailableListener(object : ImageReader.OnImageAvailableListener{
            override fun onImageAvailable(reader: ImageReader?) {
                //TODO("Not yet implemented")

                //Do stuff with image


            } }, backgroundThreadHandler)
         */

        imageReader.setOnImageAvailableListener({
            // do something
            imageReader.acquireLatestImage()?.let { image ->
            //https://www.youtube.com/watch?v=S-7H72UTiBU
                val fmt = image.format
                val time = image.timestamp
                val w = image.width
                val h = image.height

                frameCount++

                var str = "Image: $w x $h , format $fmt, time $time"

                //Log.d("Test", str)

                var buffer = image!!.planes.last().buffer
                var bytes = ByteArray(buffer.remaining())

                buffer.get(bytes)

                if(takeSnapshot){
                    //send over mqtt or store to file
                    Log.d(DEBUGTAG, "imagereader taking snapshot")
                    snapshotTakenCallback?.invoke(bytes)
                    takeSnapshot = false
                }
                else {

                    val timestampAbs = SimpleDateFormat("yyyy-MM-dd_HH:mm:ss.SSS").format(Date())

                    tsFileStream.write("$frameCount,  $timestampAbs,  $time\n")

                    if (!hasWrittenImage) {
                        mjpegFileStream.write(bytes)
                        //hasWrittenImage = true
                        //closeMJPEGFile()

                        Log.d(
                            "Test",
                            "JPEG file is written"
                        )
                    }

                }
                image.close()
            }
        }, backgroundThreadHandler)

    }

    private fun findCamera(){
        // Find the first usable front-facing camera and note the relevant characteristics.
        for (id in cameraManager.cameraIdList) {
            try {
                characteristics = cameraManager.getCameraCharacteristics(id)
            } catch (e: CameraAccessException) {
                continue
            }

            val c1 = characteristics?.get(CameraCharacteristics.FLASH_INFO_AVAILABLE)

            val facing = characteristics?.get(CameraCharacteristics.LENS_FACING)

            // We will need its best-fit supported size as well as its lens rotation so that we can
            // manage these manually in the application (since the broadcast SDK will not be handling this
            // for us, as we're not using the preset camera).
            if (characteristics?.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_BACK) {
                val size = getSupportedSize()
                val lensRotationInRadians = getLensRotation()

                if (size != null) {
                    deviceId = id
                    this.size = size
                    this.lensRotationInRadians = lensRotationInRadians

                    Toast.makeText(context, "Camera $deviceId has resolution: $size.width x $size.height", Toast.LENGTH_SHORT).show()
                    break
                }
            }
        }

        //val displayRotation = windowManager.defaultDisplay.rotation
        //var swappedDimensions = areDimensionsSwapped(displayRotation, characteristics!!)

        // swap width and height if needed
        /* val rotatedPreviewWidth = if (swappedDimensions) size.height else size.width
         val rotatedPreviewHeight = if (swappedDimensions) size.width else size.height

             previewSurfaceView.holder.setFixedSize(rotatedPreviewWidth, rotatedPreviewHeight)*/
        /*imageReader = ImageReader.newInstance(rotatedPreviewWidth, rotatedPreviewHeight,
            ImageFormat.YUV_420_888, 2)*/



        if (deviceId.isEmpty()) {
            //Log.e(DEBUGTAG, "Unable to find valid front-facing camera on this device")
        }
    }

    private fun getSupportedSize(): Size? {
        val streamConfigurationMap: StreamConfigurationMap? =
            characteristics?.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
        val sizes = streamConfigurationMap?.getOutputSizes(
            SurfaceTexture::class.java
        ) ?: return null

        var size = Size(1, 1)
        var curAspect = 1f
        val target = recordingWidth.toFloat() / recordingHeight.toFloat()
        val targetPixels = (recordingWidth * recordingHeight).toFloat()

        sizes.forEach { sz ->
            val aspect = sz.width.toFloat() / sz.height.toFloat()
            val pixels = sz.width * sz.height

            val isAspectBetter = abs(target - aspect) <= abs(target - curAspect)
            val isResolutionBetter = pixels >= size.width * size.height
            val isResolutionWithinBounds = pixels <= targetPixels

            if (isAspectBetter && isResolutionBetter && isResolutionWithinBounds) {
                curAspect = aspect
                size = sz
            }
        }

        return size
    }

    private fun getLensRotation(): Float {
        val rotation = Objects.requireNonNull<Int>(characteristics?.get(CameraCharacteristics.SENSOR_ORIENTATION))
            .toDouble()

        // Rotation needs to be reversed for the front camera.
        return -Math.toRadians(rotation).toFloat()
    }

    @SuppressLint("MissingPermission") // Permission check added in API 23
    private fun connectCamera(){
        try{
            cameraManager.openCamera(deviceId, cameraStateCallback, backgroundThreadHandler)
        }
        catch (e: CameraAccessException){
            Log.e(DEBUGTAG, e.toString())
        }
        catch (e: InterruptedException){
            Log.e(DEBUGTAG, "Open camera device interrupted while opening")

        }
    }

    private val cameraStateCallback: CameraDevice.StateCallback =
        object : CameraDevice.StateCallback() {
            override fun onOpened(camera: CameraDevice) {
                Log.d(DEBUGTAG, "camera device opened")
                if(camera!=null)
                {
                    cameraDevice = camera
                    try {
                        //previewSession()
                        //setupSession()
                        if(useImageReader){
                            imageReadingSession()
                        }
                        else{
                            recordingSession()
                        }
                    } catch (e: CameraAccessException) {
                        // handle access exception
                        Log.d("AmazonIVS", "CameraCaptureSession Caught exception $e")
                    }
                }
            }

            override fun onDisconnected(camera: CameraDevice) {
                Log.d(DEBUGTAG, "Camera disconnected")
                backgroundThreadHandler.post { safelyCloseDevice() }
            }

            override fun onError(camera: CameraDevice, error: Int) {
                Log.e(DEBUGTAG, String.format("Camera error %d", error))
                backgroundThreadHandler.post { safelyCloseDevice() }
            }
        }

    private fun imageReadingSession(){
        createImageReader()
        captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD)
        //captureBuilder.addTarget(previewSurface)
        captureBuilder.addTarget(imageReader.surface)

        cameraDevice.createCaptureSession(listOf(imageReader.surface),
            object : CameraCaptureSession.StateCallback(){
                override fun onConfigured(session: CameraCaptureSession) {
                    if(session!=null)
                    {
                        captureSession = session
                        captureBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE)

                        if(takeSnapshot){
                            captureSession.capture(captureBuilder.build(),  object : CameraCaptureSession.CaptureCallback()
                            {
                                override fun onCaptureCompleted(
                                    session: CameraCaptureSession,
                                    request: CaptureRequest,
                                    result: TotalCaptureResult
                                ) {
                                    super.onCaptureCompleted(session, request, result)
                                    //takeSnapshot = false
                                    Log.d(DEBUGTAG, "Snapshot onCaptureCompleted")
                                }

                                override fun onCaptureStarted(
                                    session: CameraCaptureSession,
                                    request: CaptureRequest,
                                    timestamp: Long,
                                    frameNumber: Long
                                ) {
                                    super.onCaptureStarted(session, request, timestamp, frameNumber)

                                    Log.d(DEBUGTAG, "Snapshot onCaptureStarted")
                                }
                            }, null)
                        }
                        else{
                            captureSession.setRepeatingRequest(captureBuilder.build(), null, null)
                            Log.d(DEBUGTAG, "starting mjpeg recording session")
                        }

                        isRecording = true



                    }
                    else{
                        Log.e(DEBUGTAG, "capture session is null")

                    }
                }

                override fun onConfigureFailed(session: CameraCaptureSession) {
                    Log.e(DEBUGTAG, "creating record session failed")

                }

            }, backgroundThreadHandler)
    }

    private fun recordingSession(){

        setupMediaRecorder()
        //val surfaceHolder = previewSurfaceView.holder
        //surfaceHolder.setFixedSize(MAX_PREVIEW_WIDTH,MAX_PREVIEW_HEIGHT)
        //val previewSurface = surfaceHolder.surface
        val recordingSurface = mediaRecorder.surface

        captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD)
        //captureBuilder.addTarget(previewSurface)
        captureBuilder.addTarget(recordingSurface)


        cameraDevice.createCaptureSession(listOf(recordingSurface),
            object : CameraCaptureSession.StateCallback(){
                override fun onConfigured(session: CameraCaptureSession) {
                    if(session!=null)
                    {
                        captureSession = session
                        captureBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE)

                        captureSession.setRepeatingRequest(captureBuilder.build(), null, null)
                        isRecording = true
                        Log.d(DEBUGTAG, "starting media recorder")

                        recordingStartedCallback?.invoke("Session configured")

                        mediaRecorder.start()
                    }
                    else{
                        Log.e(DEBUGTAG, "capture session is null")

                    }
                }

                override fun onConfigureFailed(session: CameraCaptureSession) {
                    Log.e(DEBUGTAG, "creating record session failed")

                }

            }, backgroundThreadHandler)

    }

    private fun setupMediaRecorder(){
        //val rotation = this.windowManager.defaultDisplay.rotation

        //have to do stuff with rotation and set orientationhint

        mediaRecorder.apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setVideoSource(MediaRecorder.VideoSource.SURFACE)
            setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
            setOutputFile(createVideoFile())

            Log.d(DEBUGTAG, "File location: $currentVideoFilePath")
            setVideoEncoder(MediaRecorder.VideoEncoder.H264)
            setVideoEncodingBitRate(recordingBitrate)
            setVideoFrameRate(recordingFps)
            setVideoSize(recordingWidth, recordingHeight)

            //audio settings
            //specify file type and compression format

            setAudioEncoder(MediaRecorder.AudioEncoder.AAC)

            //specify audio sampling rate and encoding bit rate (48kHz and 128kHz respectively)
            setAudioSamplingRate(48000)
            setAudioEncodingBitRate(128000)


            prepare()

        }
    }

}